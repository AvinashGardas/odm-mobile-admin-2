import database from './../Utils/database';

export const FETCH_DEVICES = 'fetch_devices';

export function getDevices() {
  return dispatch => {
    //firebase to SQL: https://firebase.googleblog.com/2013/10/queries-part-1-common-sql-queries.html
    database.ref('data/devices').on('value', snapshot => {
      dispatch({
        type: FETCH_DEVICES,
        payload: snapshot.val()
      })
    });
  }
}