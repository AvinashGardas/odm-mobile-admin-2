import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import RootReducer from './../Reducers/RootReducer';

//const logger = createLogger();

const Store = createStore(RootReducer, {}, applyMiddleware(thunk, logger));

export default Store;