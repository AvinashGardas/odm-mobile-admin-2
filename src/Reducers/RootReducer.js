//Redux provides an utility called combineReducers to compose the root reducer
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import DeviceReducer from './DeviceReducer';

const RootReducer = combineReducers(
    {
        form: formReducer,
        devices: DeviceReducer
    }
);
  
export default RootReducer;