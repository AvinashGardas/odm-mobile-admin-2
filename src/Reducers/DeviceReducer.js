import {FETCH_DEVICES} from './../Actions/NotificationsActions';

export default function (state={}, action) {
    switch(action.type) {
        case FETCH_DEVICES: return action.payload; break;

        default: return state; break;
    }
}