module.exports = {
  calcuteTimeForFeedFromNow: (feedTime) => {
      let diff = new Date().getTime() - new Date(feedTime).getTime()
      
      let diffSeconds = diff/1000
      diffSeconds = Math.round(diffSeconds)
      
      let diffMinutes = diffSeconds/60
      diffMinutes = Math.round(diffMinutes)
      
      let diffHours = diffMinutes/60
      diffHours = Math.round(diffHours)
      
      let diffInDays = diffHours/24
      diffInDays = Math.round(diffInDays)
      
      let diffInWeeks = diffInDays/7
      diffInWeeks = Math.round(diffInWeeks)
      
      let diffInMonths = diffInDays/30
      diffInMonths = Math.round(diffInMonths)
      
      let diffInYears = diffInMonths/12
      diffInYears = Math.round(diffInYears)
      
      if (diffInYears > 1) {
          return diffInYears + " years ago";
        } else if (diffInYears <= 1 && diffInMonths >= 12) {
          return diffInYears + " year ago";
        } else if (diffInYears < 0 && Math.abs(diffInYears) === 1 && Math.abs(diffInMonths) >= 12) {
          return Math.abs(diffInYears) + " year from now";
        } else if (diffInYears < 0 && Math.abs(diffInYears) > 1 && Math.abs(diffInMonths) >= 12) {
          return Math.abs(diffInYears) + " years from now";
        } else if (diffInMonths < 12 && diffInMonths > 1 && diffInWeeks > 4) {
          return diffInMonths + " months ago";
        } else if (diffInMonths === 1 && diffInWeeks > 4) {
          return diffInMonths + " month ago";
        } else if ((diffInMonths < 0) && Math.abs(diffInMonths) === 1 && Math.abs(diffInWeeks) > 4) {
          return Math.abs(diffInMonths) + " month from now";
        } else if ((diffInMonths < 0) && Math.abs(diffInMonths) > 1 && Math.abs(diffInWeeks) > 4) {
          return Math.abs(diffInMonths) + " months from now";
        } else if (diffInWeeks <= 4 && diffInWeeks > 1) {
          return diffInWeeks + " weeks ago";
        } else if (diffInWeeks === 1 && diffInDays > 7) {
          return diffInWeeks + " week ago";
        } else if ((diffInWeeks < 0) && Math.abs(diffInWeeks) === 1 && Math.abs(diffInDays) > 7) {
          return Math.abs(diffInWeeks) + " week from now";
        } else if ((diffInWeeks < 0) && Math.abs(diffInWeeks) > 1) {
          return Math.abs(diffInWeeks) + " weeks from now";
        } else if (diffInDays <= 7 && diffInDays > 1) {
          return diffInDays + " days ago";
        } else if (diffInDays <= 1 && diffHours > 24) {
          return diffInDays + " day ago";
        } else if ((diffInDays < 0) && Math.abs(diffInDays) === 1) {
          return Math.abs(diffInDays) + " day from now";
        } else if ((diffInDays < 0) && Math.abs(diffInDays) > 1) {
          return Math.abs(diffInDays) + " days from now";
        } else if ((diffHours <= 24) && (diffHours > 1)) {
          return diffHours + " hours ago";
        } else if ((diffHours <= 1) && (diffMinutes > 60)) {
          return diffHours + " hour ago";
        } else if ((diffHours < 0) && Math.abs(diffHours) === 1) {
          return Math.abs(diffHours) + " hour from now";
        } else if ((diffHours < 0) && Math.abs(diffHours) > 1) {
          return Math.abs(diffHours) + " hours from now";
        } else if ((diffMinutes <= 60) && diffMinutes > 1) {
          return diffMinutes + " minutes ago";
        } else if ((diffMinutes <= 1) && (diffSeconds > 60)) {
          return diffMinutes + " minute ago";
        } else if ((diffMinutes < 0) && Math.abs(diffMinutes) === 1) {
          return Math.abs(diffMinutes) + " minute from now";
        } else if ((diffMinutes < 0) && Math.abs(diffMinutes) > 1) {
          return Math.abs(diffMinutes) + " minutes from now";
        } else if ((diffSeconds <= 60) && (diffSeconds === 1)) {
          return diffSeconds + " second ago";
        } else if ((diffSeconds <= 60) && (diffSeconds > 0)) {
          return diffSeconds + " seconds ago";
        } else if ((diffSeconds < 0) && Math.abs(diffSeconds) === 1) {
          return Math.abs(diffSeconds) + " second from now";
        } else if ((diffSeconds < 0) && Math.abs(diffSeconds) > 1) {
          return Math.abs(diffSeconds) + " seconds from now";
        } else {
          return " just now";
        }
  },
  
  getFeedbackTime: (feedbackTimestamp) => {
    /*
      Note: timestamp structure
       2018 - 03 - 26  T  1 4  :  2 7  :  4 8  .  5 0 8  Z
       01234 567  8910   111213  141516  171819  202122 2324
    */
    
    //compare timestamps only when length equals 24
    if(feedbackTimestamp.length === 24) {
      //variables
      let currentTimestamp = new Date().toISOString()
      const feedback_year = feedbackTimestamp.substring(0,4)
      const current_year = currentTimestamp.substring(0,4)
      const feedback_month = feedbackTimestamp.substring(5,7)
      const current_month = currentTimestamp.substring(5,7)
      const feedback_day = feedbackTimestamp.substring(8,10)
      const current_day = currentTimestamp.substring(8,10)
      const feedback_hour = feedbackTimestamp.substring(11,13)
      const current_hour = currentTimestamp.substring(11,13)
      const feedback_minute = feedbackTimestamp.substring(14,16)
      const current_minute = currentTimestamp.substring(14,16)
      const feedback_second = feedbackTimestamp.substring(17,19)
      const current_second = currentTimestamp.substring(17,19)
      
      //years
      if(current_year !== feedback_year) {
        const current_year_number = parseInt(current_year)
        const feedback_year_number = parseInt(feedback_year)
        const year_diff = current_year_number-feedback_year_number
        if(year_diff > 1) {
          return year_diff+' years ago'
        } else if(year_diff < 0) {
          return year_diff+' year(s) ahead : invalid scenario'
        } else {
          return year_diff+' year ago'
        }
      } else if(current_month !== feedback_month){
        //months
        const current_month_number = parseInt(current_month)
        const feedback_month_number = parseInt(feedback_month)
        const month_diff = current_month_number - feedback_month_number
        if(month_diff > 1) {
          return month_diff+' months ago'
        } else if(month_diff < 0) {
          return month_diff+' month(s) ahead : invalid scenario'
        } else {
          return month_diff+' month ago'
        }
      } else if(current_day !== feedback_day) {
        //days
        const current_day_number = parseInt(current_day)
        const feedback_day_number = parseInt(feedback_day)
        const day_diff = current_day_number - feedback_day_number
        if(day_diff > 1) {
          return day_diff+' days ago'
        } else if(day_diff < 0) {
          return day_diff+' day(s) ago : invalid scenario'
        } else {
          return day_diff+' day ago'
        }
      } else if(current_hour !== feedback_hour) {
        //hours
        const current_hour_number = parseInt(current_hour)
        const feedback_hour_number = parseInt(feedback_hour)
        const hour_diff = current_hour_number - feedback_hour_number
        if(hour_diff > 1) {
          return hour_diff+' hours ago'
        } else if(hour_diff < 0) {
          return hour_diff+' hour(s) ahead : invalid scenario'
        } else {
          return hour_diff+' hour ago'
        }
      } else if(current_minute !== feedback_minute) {
        //minutes
        const current_minute_number = parseInt(current_minute)
        const feedback_minute_number = parseInt(feedback_minute)
        const minute_diff = current_minute_number - feedback_minute_number
        if(minute_diff > 1) {
          return minute_diff+' minutes ago'
        } else if(minute_diff < 0) {
          return minute_diff+' minute(s) ahead : invalid scenario'
        } else {
          return minute_diff+' minute ago'
        }
      } else if(current_second !== feedback_second) {
        //seconds
          const current_second_number = parseInt(current_second)
          const feedback_second_number = parseInt(feedback_second)
          const second_diff = current_second_number - feedback_second_number
          if(second_diff > 1) {
              return second_diff+' seconds ago'
          } else if(second_diff < 0) {
              return second_diff+' second(s) ahead : invalid scenario'
          } else {
              return second_diff+' second ago'
          }
      } else if(current_year === feedback_year) {
          return 'just now'
      }
      
    }//if
    else {
      return '-'
    }//else
  }  
}