import firebase from 'firebase';

//odm android app prod
var prod_config = {
  apiKey: "AIzaSyAYx5db0o2dvdJ502k1z3EOFMrUER3ilZ4",
  authDomain: "odm-android-app-eb43e.firebaseapp.com",
  databaseURL: "https://odm-android-app-eb43e.firebaseio.com",
  projectId: "odm-android-app-eb43e",
  storageBucket: "odm-android-app-eb43e.appspot.com",
  messagingSenderId: "306231514599"
};

//odm android app dev
var dev_config = {
  apiKey: "AIzaSyAdHfJklxHj-TuDDay33-X3CGDK-xy0dT4",
  authDomain: "odm-android-app-development.firebaseapp.com",
  databaseURL: "https://odm-android-app-development.firebaseio.com",
  projectId: "odm-android-app-development",
  storageBucket: "odm-android-app-development.appspot.com",
  messagingSenderId: "829712533106"
};

firebase.initializeApp(dev_config);

const database = firebase.database();

export default database;