import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
//components
import Routes from './Components/Routes';
//ui
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import * as Colors from 'material-ui/styles/colors';
//css
import './css/index.css';
import './css/fonts.css';

injectTapEventPlugin();

const muiTheme = getMuiTheme(
    {
        palette: {
            primary1Color: Colors.grey800,
            textColor: Colors.darkBlack,
            alternateTextColor: Colors.white,
            accent1Color: Colors.white
        }
    }
)

const Root = () => (
    <MuiThemeProvider muiTheme={muiTheme}>
        <Routes />
    </MuiThemeProvider>
);

export default Root;
registerServiceWorker();