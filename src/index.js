import React from 'react';
import ReactDOM from 'react-dom';
//The Provider component is the top-level component within a React application.
import { Provider } from 'react-redux';
import Root from './Root';

//stores
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
//reducers
import reducers from './Reducers/RootReducer';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

const App = () => (
    <Provider store={createStoreWithMiddleware(reducers)}>
        <Root />
    </Provider>
  );

ReactDOM.render(<App />, document.getElementById('root'));
