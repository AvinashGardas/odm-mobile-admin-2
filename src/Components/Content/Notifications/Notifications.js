import React, {Component} from 'react';
import firebase from 'firebase';
//redux
import { connect } from 'react-redux';
import { Field, reduxForm, reset } from 'redux-form';
//lodash
import _ from 'lodash';
//actions
import { getDevices } from './../../../Actions/NotificationsActions';
//components
import DevicesList from './DevicesList';
//ui
import {List} from 'material-ui/List';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import * as Colors from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import SendIcon from 'material-ui/svg-icons/content/send';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
//css
import './notifications.css';

let index;

function NotificationsPanel(props) {
    return(
        <div className="notifications-panel">
            <TextField
            id="notification-title"
            hintText="Title"
            floatingLabelText="Title"
            fullWidth={true}
            />
            <br/>

            <TextField
            id="notification-message"
            hintText="Message"
            floatingLabelText="Message"
            fullWidth={true}
            />
            <br />

            <RaisedButton 
                label="Send All"
                labelPosition="before"
                icon={<SendIcon color={Colors.black}/>}
                onTouchTap={props.handleDialogOpen}/>
        </div>
    )
}

class Notifications extends Component {
    constructor(props) {
        super(props);
        this.state = {
          openSnackbar: false,
          notificationStatus: '',
          openSendAllNotificationsDialog: false
        };
      }

    componentWillMount() {
        //call getDevices() before component will mount
        this.props.getDevices()
    }

    sendNotificationToOneDevice = (deviceName, deviceToken) => {
        const title = document.getElementById("notification-title").value;
        const message = document.getElementById("notification-message").value;

        this.sendNotification(deviceName, deviceToken, title, message, 'single');
    }

    sendNotificationToAllDevices = () => {
        const title = document.getElementById("notification-title").value;
        const message = document.getElementById("notification-message").value;

        _.map(this.props.devices, (device, key) => {
            let deviceToken = device.deviceToken;
            let deviceName = device.deviceName;
            
            this.sendNotification(deviceName, deviceToken, title, message, 'all');
        });

        //close dialog
        this.handleDialogClose();
    }

    sendNotification = (deviceName, deviceToken, title, message, type) => {
        
        const ODM_Android = 'key=AAAAR0zSFec:APA91bFIffH7BcenCr8Ppe7wOrga0DEQk_guNtkKA9XPt5pJRSNPMNXC6naixk8y9lWsIazjunkDO_SUJOLzI4Dn3mQpKFgOy_mlh8p0oygDUkoN_8FI_FxajRTUNgijkoIz6XPyUXl8';
        const epicbase = 'key=AAAARGxbfW8:APA91bElC5mW0qmiDS1RSFSozZ_TrtsirIfVpOO1SWjaa_uKbDXIGD-dWBNU29qp4dlEtIrmlg1V4vjLhlUOmM69K-nKFbND4xtxaCawIr0vxUJVIWi7EmVgiZg-oPbG7BrfKspGO6lD';

        fetch('https://fcm.googleapis.com/fcm/send', {
            method: 'POST',
            headers: {
                'Authorization': ODM_Android,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                to: deviceToken,
                notification: {
                    body: message
                }
            })
        })
        .then((res) => res.json)
        .then((res) => {
            //snackbar
            if(type === 'single') {
                this.setState({notificationStatus: `Notification sent to ${deviceName}`});
                //show snackbar
                this.setState({openSnackbar: true});
            }
            else {
                this.setState({notificationStatus: `Notification sent to all devices`});
                //show snackbar
                this.setState({openSnackbar: true});
            }
        })
        .catch((error) => {
            this.setState({notificationStatus: `Error in sending notification!`});
            //show snackbar
            this.setState({openSnackbar: true});
        })
    }

    handleTouchTap = () => {
        this.setState({
          openSnackbar: true,
        });
      };
    
    handleRequestClose = () => {
        this.setState({
          openSnackbar: false,
        });
      };

    handleDialogOpen = () => {
        this.setState({
            openSendAllNotificationsDialog: true,
        });
    };

    handleDialogClose = () => {
        this.setState({
            openSendAllNotificationsDialog: false,
        });
    };
    
    renderDevices() {
        return _.map(this.props.devices, (device, key) => {
            index = device.index;

            return(
                <DevicesList data={device} key={key} sendNotificationToOneDevice={this.sendNotificationToOneDevice}/>
            )
        })
    }

    render() {
        const dialogActions = [
            <FlatButton
              label="No"
              onClick={this.handleDialogClose}
            />,
            <FlatButton
              label="Yes"
              onClick={this.sendNotificationToAllDevices}
            />,
          ];

        return(
            <div id="page-container" className="page-container">

                <NotificationsPanel handleDialogOpen={this.handleDialogOpen}/>
                
                <List>
                    {this.renderDevices()}
                </List>
                
                <Snackbar
                style={{color: Colors.white}}
                open={this.state.openSnackbar}
                message={this.state.notificationStatus}
                autoHideDuration={5000}
                onRequestClose={this.handleRequestClose}
                />

                <Dialog
                title="Notifications"
                actions={dialogActions}
                modal={true}
                open={this.state.openSendAllNotificationsDialog}
                >
                    Do you wanna send notification to all devices?
                </Dialog>
            </div>
        )
    }
}

let form = reduxForm({
    form: 'Notifications'
})(Notifications);

form = connect(state => ({
    devices: state.devices
}), {getDevices})(form);

export default form;