import React, {Component} from 'react';
//ui
import Avatar from 'material-ui/Avatar';
import AndroidIcon from 'material-ui/svg-icons/action/android';
import {ListItem} from 'material-ui/List';
import SendIcon from 'material-ui/svg-icons/content/send';
import * as Colors from 'material-ui/styles/colors';

let index;

function DevicesItem(props) {
    const device = props.data;
    return(
        <ListItem
        leftAvatar={<Avatar icon={<AndroidIcon color={Colors.white}/>} backgroundColor={Colors.green500}/>}
        rightIcon={<SendIcon onClick={ () => {props.sendNotificationToOneDevice(device.deviceName, device.deviceToken)} }/>}
        primaryText={device.deviceName}
        secondaryText={device.deviceVersion}
      />
    )
}

class DevicesList extends Component {

    render() {
        return(
            <DevicesItem data={this.props.data} sendNotificationToOneDevice={this.props.sendNotificationToOneDevice}/>
        )
    }
}
export default DevicesList;