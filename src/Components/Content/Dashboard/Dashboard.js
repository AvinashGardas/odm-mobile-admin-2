import React, {Component} from 'react'
//components
import TinyMceEditor from './../../GenericComponents/TinyMceEditor/TinyMceEditor'
import UserRequests from './../../GenericComponents/UserRequests/UserRequests'
import ToDoList from './../../GenericComponents/ToDoList/ToDoList'
//ui
import {Card} from 'material-ui/Card'
//css
import './../css/content.css'

class Dashboard extends Component {
    render() {
        return(
            <div id="page-container">
                
                <div className="masonry-container">
                    <div className="masonry">
                        <div className="masonry-item">
                            <Card style={{padding: 8}}>
                                <div className="display-flex-center">
                                    <span className="roboto-slab" style={{fontSize: 20, padding: 8}}>Welcome to ODM Admin</span>
                                </div>
                            </Card>
                        </div>

                        <div className="masonry-item">
                            <ToDoList />
                        </div>

                        <div className="masonry-item">
                            <UserRequests />
                        </div>
                    </div>
                </div>

                <div className="pure-g">
                    <div className="pure-u-1 pure-u-md-12-24">
                        {/* to do list */}
                        <TinyMceEditor />
                    </div>

                    <div className="pure-u-1 pure-u-md-12-24">
                        {/* users requests */}
                        
                    </div>
                </div>

            </div>
        )
    }
}
export default Dashboard