import React, {Component} from 'react';
//components
import StorageList from './../../Example/StorageList'
import ThumbnailsStorageList from './../../Example/ThumbnailsStorageList'
import {Tabs, Tab} from 'material-ui/Tabs'

class Feed extends Component {
    render() {
        return(
            <div id="page-container" className="page-container">
                <Tabs
                    className="tabs-generic">
                    <Tab label="Photos" value="photos">
                        <StorageList />
                    </Tab>

                    <Tab label="Thumbnails" value="thumbnails">
                        <ThumbnailsStorageList />
                    </Tab>  
                </Tabs>
                
            </div>
        )
    }
}
export default Feed;