import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
//components
import ExperimentComponent from '../../Example/ExperimentComponent'
import ExperimentAddMediaPhotoForPath from '../../Example/ExperimentAddMediaPhotoForPath'
import ExperimentAddImageCarouselDataForPath from '../../Example/ExperimentAddImageCarouselDataForPath'
import ExperimentAddPhotoMetadata from '../../Example/ExperimentAddPhotoMetadata'
import StorageList from './../../Example/StorageList'
//ui
import {Tabs, Tab} from 'material-ui/Tabs'
//css
import './../css/content.css'

class Media extends Component {
    constructor(props) {
        super(props)
        this.state = {
          value: this.props.match.params.type,
        }
    }

    componentDidMount() {
        this.handleLocation(this.props)
    }

    componentWillReceiveProps(nextProps) {
        this.handleLocation(nextProps)
    }

    handleChange = (value) => {
        this.setState({
          value: value,
        })
    }

    handleParams = (props) => {
        const type = props.match.params.type
        this.setState({value: type})
    }

    handleLocation = (props) => {
        //const location = props.location.pathname
        const type = props.match.params.type

        if(type === 'music') {
            this.setState({value: 'photos'})
        } else if (type === 'photos')  {
            this.setState({value: 'photos'})
        } else if (type === 'videos') {
            this.setState({value: 'videos'})
        }
        else if (type === null || type === undefined || type === ''){
            //default
            this.setState({value: 'photos'})
        } else {
            //default
            this.setState({value: 'photos'})
        }
    }

    render() {
        return(
            <div id="page-container" className="page-container">
                    <Tabs
                    className="tabs-generic"
                    value={this.state.value}
                    onChange={this.handleChange}>
                        <Tab label="Music" value="music">

                        </Tab>

                        <Tab label="Photos" value="photos">
                            <ExperimentComponent />
                            <ExperimentAddMediaPhotoForPath />
                            <ExperimentAddImageCarouselDataForPath />
                        </Tab>

                        <Tab label="Videos" value="videos">
                            
                        </Tab>
                    </Tabs>
    
                
            </div>
        )
    }
}
export default withRouter(Media)