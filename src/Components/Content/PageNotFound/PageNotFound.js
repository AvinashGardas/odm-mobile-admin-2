import React, {Component} from 'react';
//css
import './css/pagenotfound.css';

class PageNotFound extends Component {
    render() {
        return(
            <div>

                <div className="bgimg" style={{marginTop: 0}}>
                        <div className="middle">
                            <h1>Page not found</h1>
                            <hr/>
                            <p>404</p>
                        </div>
                    </div>

            </div>
        )
    }
}
export default PageNotFound;