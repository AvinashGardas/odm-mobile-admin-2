import React,{Component} from 'react'
import firebase from 'firebase'
import {withRouter} from 'react-router-dom'
//components
import WelcomeCarousel from './WelcomeCarousel'
import SignIn from './../../Container/SignIn/SignIn'
//ui
import * as Colors from 'material-ui/styles/colors'
//css
import './css/welcome.css'

class Welcome extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            authenticated: false,
            Loading: true
        }
    }

    componentDidMount() {
        this.reCheckAuth()
        this.redirectToSignIn()
    }

    componentWillReceiveProps() {
        this.reCheckAuth()
        this.redirectToSignIn()
    }

    reCheckAuth = () => {
        firebase.auth().onAuthStateChanged(user => {
            if(user !== null) {
                this.setState({user: user.email, authenticated: true, Loading: false})
            } else {
                this.setState({user: null, authenticated: false, Loading: true})
            }
        })
    }

    redirectToSignIn = () => {
        //if user is not signed in, redirect to signin
        if(this.state.user === null) {
            this.props.history.push('/signin')
        }
        //else if user is signed in, and if clicks on home route, then redirect to dashboard
        //it is already handled in navigation bar
    }

    calculateCountDown = () => {
        let countDown = new Date('July 1, 2018 00:00:00').getTime()
        let now = new Date().getTime()
        let distance = countDown - now
        
        if(distance < 0) {
            clearInterval(this.state.intervalId)
            this.setState({time: 'Stay tuned!'})
        } else {
            var days = Math.floor(distance / (1000 * 60 * 60 * 24))
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
            var seconds = Math.floor((distance % (1000 * 60)) / 1000)

            this.setState({time: `${days}d ${hours}h ${minutes}m ${seconds}s`})
        }
    }

    render() {

        return(
            <div>
                {/* <div className="bgimg" style={{marginTop: 0}}>
                        <div className="topleft">
                            <p>ODM Mobile Admin</p>
                        </div>
                        <div className="middle">
                            <h1>COMING SOON</h1>
                            <hr/>
                            <p>{this.state.time}</p>
                        </div>
                    </div> */}

                    <div>Welcome!</div>
                
            </div>
        )
    }
}
export default withRouter(Welcome)