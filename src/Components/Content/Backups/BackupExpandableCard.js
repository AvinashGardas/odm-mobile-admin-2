import React, {Component} from 'react'
import firebase from 'firebase'
//ui
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'
import * as Colors from 'material-ui/styles/colors'
//components
import BackupList from './BackupList'

// ODM backup - firebase
var odm_backup_config = {
    apiKey: "AIzaSyDETKYhzQxW98DbY49WwX4PMXwb2n9JzmY",
    authDomain: "odm-web-app-prod-restore.firebaseapp.com",
    databaseURL: "https://odm-web-app-prod-restore.firebaseio.com",
    projectId: "odm-web-app-prod-restore",
    storageBucket: "odm-web-app-prod-restore.appspot.com",
    messagingSenderId: "1012852084684"
}

//firebase app initializations
var odm_backup_initializeApp = firebase.initializeApp(odm_backup_config, "odm-backup")

//databases
var odm_backup_database = odm_backup_initializeApp.database()

class BackupExpandableCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            odm_backup_data: [],
            lastBackupDetails: ''
        }
    }

    componentDidMount() {
        this.getDataFromPath(this.props.backupDataPath)
        this.getLastBackedupDataDetails(this.props.backupDataPath)
    }

    getDataFromPath = (path) => {
        const odm_backup_ref = odm_backup_database.ref(path)
        odm_backup_ref.once('value', snapshot => {
            this.setState({odm_backup_data: snapshot.val()})
        })
    }

    getLastBackedupDataDetails = (path) => {
        const odm_backup_ref = odm_backup_database.ref(path)
        const lastBackup = odm_backup_ref.limitToLast(1).on('child_added', snapshot => {
            const item_timestamp = snapshot.val().timestamp
            this.setState({lastBackupDetails: item_timestamp})
        })
    }

    backupData = () => {
        const path = this.props.backupDataPath
        const odm_backup_ref = odm_backup_database.ref(path)

        const key = odm_backup_ref.push().key
        const timestamp = new Date().toISOString()
        const data = this.props.data

        const backupObject = {
            id: key,
            timestamp: timestamp,
            data: data
        }

        odm_backup_ref.child(key).set(backupObject)
                        .then(res => {
                            
                        })
                        .catch(err => {

                        })
    }

    calcuteTimeForFeedFromNow = (feedTime) => {
        let diff = new Date().getTime() - new Date(feedTime).getTime()
        
        let diffSeconds = diff/1000
        diffSeconds = Math.round(diffSeconds)
        
        let diffMinutes = diffSeconds/60
        diffMinutes = Math.round(diffMinutes)
        
        let diffHours = diffMinutes/60
        diffHours = Math.round(diffHours)
        
        let diffInDays = diffHours/24
        diffInDays = Math.round(diffInDays)
        
        let diffInWeeks = diffInDays/7
        diffInWeeks = Math.round(diffInWeeks)
        
        let diffInMonths = diffInDays/30
        diffInMonths = Math.round(diffInMonths)
        
        let diffInYears = diffInMonths/12
        diffInYears = Math.round(diffInYears)
        
        if (diffInYears > 1) {
            return diffInYears + " years ago";
          } else if (diffInYears <= 1 && diffInMonths >= 12) {
            return diffInYears + " year ago";
          } else if (diffInYears < 0 && Math.abs(diffInYears) === 1 && Math.abs(diffInMonths) >= 12) {
            return Math.abs(diffInYears) + " year from now";
          } else if (diffInYears < 0 && Math.abs(diffInYears) > 1 && Math.abs(diffInMonths) >= 12) {
            return Math.abs(diffInYears) + " years from now";
          } else if (diffInMonths < 12 && diffInMonths > 1 && diffInWeeks > 4) {
            return diffInMonths + " months ago";
          } else if (diffInMonths === 1 && diffInWeeks > 4) {
            return diffInMonths + " month ago";
          } else if ((diffInMonths < 0) && Math.abs(diffInMonths) === 1 && Math.abs(diffInWeeks) > 4) {
            return Math.abs(diffInMonths) + " month from now";
          } else if ((diffInMonths < 0) && Math.abs(diffInMonths) > 1 && Math.abs(diffInWeeks) > 4) {
            return Math.abs(diffInMonths) + " months from now";
          } else if (diffInWeeks <= 4 && diffInWeeks > 1) {
            return diffInWeeks + " weeks ago";
          } else if (diffInWeeks === 1 && diffInDays > 7) {
            return diffInWeeks + " week ago";
          } else if ((diffInWeeks < 0) && Math.abs(diffInWeeks) === 1 && Math.abs(diffInDays) > 7) {
            return Math.abs(diffInWeeks) + " week from now";
          } else if ((diffInWeeks < 0) && Math.abs(diffInWeeks) > 1) {
            return Math.abs(diffInWeeks) + " weeks from now";
          } else if (diffInDays <= 7 && diffInDays > 1) {
            return diffInDays + " days ago";
          } else if (diffInDays <= 1 && diffHours > 24) {
            return diffInDays + " day ago";
          } else if ((diffInDays < 0) && Math.abs(diffInDays) === 1) {
            return Math.abs(diffInDays) + " day from now";
          } else if ((diffInDays < 0) && Math.abs(diffInDays) > 1) {
            return Math.abs(diffInDays) + " days from now";
          } else if ((diffHours <= 24) && (diffHours > 1)) {
            return diffHours + " hours ago";
          } else if ((diffHours <= 1) && (diffMinutes > 60)) {
            return diffHours + " hour ago";
          } else if ((diffHours < 0) && Math.abs(diffHours) === 1) {
            return Math.abs(diffHours) + " hour from now";
          } else if ((diffHours < 0) && Math.abs(diffHours) > 1) {
            return Math.abs(diffHours) + " hours from now";
          } else if ((diffMinutes <= 60) && diffMinutes > 1) {
            return diffMinutes + " minutes ago";
          } else if ((diffMinutes <= 1) && (diffSeconds > 60)) {
            return diffMinutes + " minute ago";
          } else if ((diffMinutes < 0) && Math.abs(diffMinutes) === 1) {
            return Math.abs(diffMinutes) + " minute from now";
          } else if ((diffMinutes < 0) && Math.abs(diffMinutes) > 1) {
            return Math.abs(diffMinutes) + " minutes from now";
          } else if ((diffSeconds <= 60) && (diffSeconds === 1)) {
            return diffSeconds + " second ago";
          } else if ((diffSeconds <= 60) && (diffSeconds > 0)) {
            return diffSeconds + " seconds ago";
          } else if ((diffSeconds < 0) && Math.abs(diffSeconds) === 1) {
            return Math.abs(diffSeconds) + " second from now";
          } else if ((diffSeconds < 0) && Math.abs(diffSeconds) > 1) {
            return Math.abs(diffSeconds) + " seconds from now";
          } else if (feedTime === '') {
            return "";
          } else {
            return " just now";
          }
    }

    render() {
        const lastBackupTime = this.calcuteTimeForFeedFromNow(this.state.lastBackupDetails)

        return(
            <Card>
                <CardHeader
                    title={this.props.title}
                    subtitle={<span>Last backup <i className="far fa-clock"></i> {lastBackupTime}</span>}
                    actAsExpander={true}
                    showExpandableButton={true}
                />

                <CardActions>
                    <FlatButton 
                        label="Backup"
                        primary={true}
                        onClick={this.backupData}
                    />
                </CardActions>

                <CardText expandable={true}>
                    <BackupList
                        title={this.props.title}
                        databaseName={this.props.databaseName}
                        backupDataPath={this.props.backupDataPath}
                        odm_backup_database={odm_backup_database}
                    />
                </CardText>
            </Card>
        )
    }
}

export default BackupExpandableCard