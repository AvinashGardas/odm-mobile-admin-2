import React, {Component} from 'react'
import firebase from 'firebase'
//components
import BackupExpandableCard from './BackupExpandableCard'

// ODM Web App Prod - firebase
var odm_web_app_prod_config = {
    apiKey: "AIzaSyAeeOm8QJdYwwe6WbXWRnQY2S3mPJkv8WM",
    authDomain: "odm-web-app-prod.firebaseapp.com",
    databaseURL: "https://odm-web-app-prod.firebaseio.com",
    projectId: "odm-web-app-prod",
    storageBucket: "odm-web-app-prod.appspot.com",
    messagingSenderId: "512401827800"
}

var odm_android_app_dev_config = {
    apiKey: "AIzaSyAdHfJklxHj-TuDDay33-X3CGDK-xy0dT4",
    authDomain: "odm-android-app-development.firebaseapp.com",
    databaseURL: "https://odm-android-app-development.firebaseio.com",
    projectId: "odm-android-app-development",
    storageBucket: "odm-android-app-development.appspot.com",
    messagingSenderId: "829712533106"
}

var odm_android_app_prod_config = {
    apiKey: "AIzaSyAYx5db0o2dvdJ502k1z3EOFMrUER3ilZ4",
    authDomain: "odm-android-app-eb43e.firebaseapp.com",
    databaseURL: "https://odm-android-app-eb43e.firebaseio.com",
    projectId: "odm-android-app-eb43e",
    storageBucket: "odm-android-app-eb43e.appspot.com",
    messagingSenderId: "306231514599"
}

//firebase app initializations
var odm_web_app_prod_initializeApp = firebase.initializeApp(odm_web_app_prod_config, "odm-web-app-prod")
var odm_android_app_dev_initializeApp = firebase.initializeApp(odm_android_app_dev_config, "odm-android-app-dev")
var odm_android_app_prod_initializeApp = firebase.initializeApp(odm_android_app_prod_config, "odm-android-app-prod")

//databases
var odm_web_app_prod_database = odm_web_app_prod_initializeApp.database()
var odm_android_app_dev_database = odm_android_app_dev_initializeApp.database()
var odm_android_app_prod_database = odm_android_app_prod_initializeApp.database()

class Backups extends Component {
    constructor(props) {
        super(props)
        this.state = {
            odm_web_app_prod_data: [],
            odm_web_app_dev_data: [],

            odm_android_app_dev_data: [],
            odm_android_app_prod_data: []
        }
    }

    componentDidMount() {
        //odm-web-prod
        this.getOdmWebProdData()
        //odm-web-dev
        this.getOdmWebDevData()

        //odm-android-prod
        this.getOdmAndroidProdData()
        //odm-android-dev
        this.getOdmAndroidDevData()
    }

    getOdmWebProdData() {
        const odm_web_app_prod_ref = odm_web_app_prod_database.ref('/')
        odm_web_app_prod_ref.on('value', (snapshot) => {
            this.setState({odm_web_app_prod_data: snapshot.val()})
        })
    }

    getOdmWebDevData() {}

    getOdmAndroidDevData() {
        const odm_android_app_dev_ref = odm_android_app_dev_database.ref('/')
        odm_android_app_dev_ref.on('value', (snapshot) => {
            this.setState({odm_android_app_dev_data: snapshot.val()})
        })
    }

    getOdmAndroidProdData() {
        const odm_android_app_prod_ref = odm_android_app_prod_database.ref('/')
        odm_android_app_prod_ref.on('value', (snapshot) => {
            this.setState({odm_android_app_prod_data: snapshot.val()})
        })
    }

    render() {
        return(
            <div id="page-container" className="page-container">
                {/* Backup card: ODM Android Dev */}
                <BackupExpandableCard 
                title={'ODM Android Development'}
                databaseName={'odm-android-app-development'}
                data={this.state.odm_android_app_dev_data}
                backupDataPath={'/odm-android/dev/'}
                />
                <br/>

                {/* Backup card: ODM Android Prod */}
                <BackupExpandableCard 
                title={'ODM Android Prod'}
                databaseName={'odm-android-app-prod'}
                data={this.state.odm_android_app_prod_data}
                backupDataPath={'/odm-android/prod/'}
                />
                <br/>

                {/* Backup card: ODM Web Prod */}
                <BackupExpandableCard 
                title={'ODM Web Prod'}
                databaseName={'odm-web-app-prod'}
                data={this.state.odm_web_app_prod_data}
                backupDataPath={'/odm-web/prod/'}
                />
                <br/>
            </div>
        )
    }
}

export default Backups