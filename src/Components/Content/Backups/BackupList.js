import React, {Component} from 'react'
import firebase from 'firebase'
import JSONTree from 'react-json-tree'
//ui
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table'
import IconButton from 'material-ui/IconButton'
import InsertDriveFileIcon from 'material-ui/svg-icons/editor/insert-drive-file'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import FlatButton from 'material-ui/FlatButton'
import Dialog from 'material-ui/Dialog'

//base00 is background-color, if invertTheme is true, #000000 becomes #ffffff
const json_tree_theme = {
    scheme: 'monokai',
    author: 'wimer hazenberg (http://www.monokai.nl)',
    base00: '#000000',
    base01: '#383830',
    base02: '#49483e',
    base03: '#75715e',
    base04: '#a59f85',
    base05: '#f8f8f2',
    base06: '#f5f4f1',
    base07: '#f9f8f5',
    base08: '#f92672',
    base09: '#fd971f',
    base0A: '#f4bf75',
    base0B: '#a6e22e',
    base0C: '#a1efe4',
    base0D: '#66d9ef',
    base0E: '#ae81ff',
    base0F: '#cc6633'
}

class BackupList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: '',
            dialogData: '',
            openDialog: false
        }
    }

    componentDidMount() {
        this.getDataFromPath(this.props.backupDataPath)
    }

    getDataFromPath = (path) => {
        const odm_backup_database = this.props.odm_backup_database

        const odm_backup_ref = odm_backup_database.ref(path)
        odm_backup_ref.on('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    deleteBackup = (id) => {
        const path = this.props.backupDataPath+id
        const odm_backup_database = this.props.odm_backup_database

        odm_backup_database.ref(path).remove()
    }

    calcuteTimeForFeedFromNow = (feedTime) => {
        let diff = new Date().getTime() - new Date(feedTime).getTime()
        
        let diffSeconds = diff/1000
        diffSeconds = Math.round(diffSeconds)
        
        let diffMinutes = diffSeconds/60
        diffMinutes = Math.round(diffMinutes)
        
        let diffHours = diffMinutes/60
        diffHours = Math.round(diffHours)
        
        let diffInDays = diffHours/24
        diffInDays = Math.round(diffInDays)
        
        let diffInWeeks = diffInDays/7
        diffInWeeks = Math.round(diffInWeeks)
        
        let diffInMonths = diffInDays/30
        diffInMonths = Math.round(diffInMonths)
        
        let diffInYears = diffInMonths/12
        diffInYears = Math.round(diffInYears)
        
        if (diffInYears > 1) {
            return diffInYears + " years ago";
          } else if (diffInYears <= 1 && diffInMonths >= 12) {
            return diffInYears + " year ago";
          } else if (diffInYears < 0 && Math.abs(diffInYears) === 1 && Math.abs(diffInMonths) >= 12) {
            return Math.abs(diffInYears) + " year from now";
          } else if (diffInYears < 0 && Math.abs(diffInYears) > 1 && Math.abs(diffInMonths) >= 12) {
            return Math.abs(diffInYears) + " years from now";
          } else if (diffInMonths < 12 && diffInMonths > 1 && diffInWeeks > 4) {
            return diffInMonths + " months ago";
          } else if (diffInMonths === 1 && diffInWeeks > 4) {
            return diffInMonths + " month ago";
          } else if ((diffInMonths < 0) && Math.abs(diffInMonths) === 1 && Math.abs(diffInWeeks) > 4) {
            return Math.abs(diffInMonths) + " month from now";
          } else if ((diffInMonths < 0) && Math.abs(diffInMonths) > 1 && Math.abs(diffInWeeks) > 4) {
            return Math.abs(diffInMonths) + " months from now";
          } else if (diffInWeeks <= 4 && diffInWeeks > 1) {
            return diffInWeeks + " weeks ago";
          } else if (diffInWeeks === 1 && diffInDays > 7) {
            return diffInWeeks + " week ago";
          } else if ((diffInWeeks < 0) && Math.abs(diffInWeeks) === 1 && Math.abs(diffInDays) > 7) {
            return Math.abs(diffInWeeks) + " week from now";
          } else if ((diffInWeeks < 0) && Math.abs(diffInWeeks) > 1) {
            return Math.abs(diffInWeeks) + " weeks from now";
          } else if (diffInDays <= 7 && diffInDays > 1) {
            return diffInDays + " days ago";
          } else if (diffInDays <= 1 && diffHours > 24) {
            return diffInDays + " day ago";
          } else if ((diffInDays < 0) && Math.abs(diffInDays) === 1) {
            return Math.abs(diffInDays) + " day from now";
          } else if ((diffInDays < 0) && Math.abs(diffInDays) > 1) {
            return Math.abs(diffInDays) + " days from now";
          } else if ((diffHours <= 24) && (diffHours > 1)) {
            return diffHours + " hours ago";
          } else if ((diffHours <= 1) && (diffMinutes > 60)) {
            return diffHours + " hour ago";
          } else if ((diffHours < 0) && Math.abs(diffHours) === 1) {
            return Math.abs(diffHours) + " hour from now";
          } else if ((diffHours < 0) && Math.abs(diffHours) > 1) {
            return Math.abs(diffHours) + " hours from now";
          } else if ((diffMinutes <= 60) && diffMinutes > 1) {
            return diffMinutes + " minutes ago";
          } else if ((diffMinutes <= 1) && (diffSeconds > 60)) {
            return diffMinutes + " minute ago";
          } else if ((diffMinutes < 0) && Math.abs(diffMinutes) === 1) {
            return Math.abs(diffMinutes) + " minute from now";
          } else if ((diffMinutes < 0) && Math.abs(diffMinutes) > 1) {
            return Math.abs(diffMinutes) + " minutes from now";
          } else if ((diffSeconds <= 60) && (diffSeconds === 1)) {
            return diffSeconds + " second ago";
          } else if ((diffSeconds <= 60) && (diffSeconds > 0)) {
            return diffSeconds + " seconds ago";
          } else if ((diffSeconds < 0) && Math.abs(diffSeconds) === 1) {
            return Math.abs(diffSeconds) + " second from now";
          } else if ((diffSeconds < 0) && Math.abs(diffSeconds) > 1) {
            return Math.abs(diffSeconds) + " seconds from now";
          } else if (feedTime === '') {
            return "";
          } else {
            return " just now";
          }
    }
    
    renderList() {
        const data = this.state.data

        return (
            <Table
                selectable={false}
            >
                <TableHeader
                    displaySelectAll={false}
                    adjustForCheckbox={false}
                >
                    <TableRow>
                        <TableHeaderColumn>File Name</TableHeaderColumn>
                        <TableHeaderColumn>Date (YY-MM-DD)</TableHeaderColumn>
                        <TableHeaderColumn>Time (GMT)</TableHeaderColumn>
                        <TableHeaderColumn>Status</TableHeaderColumn>
                        <TableHeaderColumn>Delete</TableHeaderColumn>
                        <TableRowColumn></TableRowColumn>
                    </TableRow>
                </TableHeader>

                <TableBody
                    displayRowCheckbox={false}
                >
                {   data !== null ?
                    Object.values(data).map((item, key) => {
                        return(
                            <TableRow key={key}>
                                <TableRowColumn>
                                    <InsertDriveFileIcon /> <span>{this.props.databaseName}.json</span>
                                </TableRowColumn>

                                <TableRowColumn>
                                    {item.timestamp.substring(0,10)}
                                </TableRowColumn>

                                <TableRowColumn>
                                {item.timestamp.substring(11,16)}
                                </TableRowColumn>

                                <TableRowColumn>
                                    <i className="far fa-clock"></i> {this.calcuteTimeForFeedFromNow(item.timestamp)}
                                </TableRowColumn>

                                <TableRowColumn>
                                    <IconButton
                                        onClick={()=>{this.deleteBackup(item.id)}}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </TableRowColumn>

                                <TableRowColumn>
                                    <FlatButton
                                        label='View'
                                        onClick={()=>this.displayJsonData(item.id)}
                                    />
                                </TableRowColumn>
                            </TableRow>
                        )
                    })
                    :
                    <TableRow>
                        <TableRowColumn style={{textAlign: 'center'}}>
                            No backups found
                        </TableRowColumn>
                    </TableRow>
                }
                </TableBody>
            </Table>
        )
    }

    displayJsonData = (id) => {
        const path = this.props.backupDataPath+id
        const odm_backup_database = this.props.odm_backup_database

        const odm_backup_ref = odm_backup_database.ref(path)
        odm_backup_ref.once('value', snapshot => {
            this.setState({dialogData: snapshot.val()})
        })

        this.setState({openDialog: true})
    }

    handleDialogClose = () => {
        this.setState({openDialog: false})
    }

    render() {
        const actions = [
            <FlatButton
              label="Close"
              primary={true}
              onClick={this.handleDialogClose}
            />
        ]
        const dialogData = this.state.dialogData
        let data = dialogData.data

        return(
            <div>
                {
                    this.state.data !== '' ?
                    this.renderList()
                    :
                    <span>Loading...</span>
                }

                <Dialog
                title={this.props.title}
                actions={actions}
                modal={false}
                open={this.state.openDialog}
                onRequestClose={this.handleDialogClose}
                autoScrollBodyContent={true}
                contentStyle={{width: `100%`, height: `100%`}}
                >
                    <div>
                        <JSONTree 
                        data={data} 
                        theme={json_tree_theme} 
                        invertTheme={true} 
                        />
                        {/* {JSON.stringify(data, null, 2)} */}
                    </div>
                </Dialog>
            </div>
        )
    }
}
export default BackupList