import React, {Component} from 'react'
import PropTypes from 'prop-types'
//css
import './css/authentication.css'
//import './scss/authentication.scss'
//comp
import LoginView from './LoginView'
import BlurOverlay from './BlurOverlay'

class Authentication extends Component {
    constructor(props) {
        super(props)

        this.state = {
            overlayOpen: false
        }
    }

    toggleBlurOverlay = () => {
        this.setState({overlayOpen: !this.state.overlayOpen})
    }

    render() {
        return(
            <div className="authentication-view-container">
                <button onClick={this.toggleBlurOverlay}>Toggle</button>
                
                {/* <LoginView /> */}
                
                <BlurOverlay 
                overlayToggle={this.state.overlayOpen}
                toggleBlurOverlay={this.toggleBlurOverlay} />
            </div>
        )
    }
}

export default Authentication