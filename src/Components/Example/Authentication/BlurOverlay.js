import React, {Component} from 'react'
//scss
//import './scss/blur-overlay.scss'
//import './scss/authentication.scss'
import './css/authentication.css'

class BlurOverlay extends Component {
    render() {
        return(
            <div className="blur-overlay-container" style={{display: this.props.overlayToggle ? 'block' : 'none'}}>
                <form action="">
                    <img src="https://bit.ly/2tlJLoz"/>
                    <br/>
                    <input type="text"/>
                    <br/>
                    <input type="password"/>
                    <br/>
                    <input className="form-button" value="CLOSE" onClick={this.props.toggleBlurOverlay} />
                    <br/>
                    <span><a href="#">Forgot Password?</a></span>
                </form>
            </div>
        )
    }
}

export default BlurOverlay