import React, {Component} from 'react'
import './css/card.css'

class Card extends Component {
    render() {
        return(
            <div>
                <div className="card front">
                <div className="blue"></div>
                <div className="yellow"></div>
                <div className="pink"></div>
                <div className="dots"></div>
                <div className="personal-intro">
                    <p>Krista Stone</p>
                    <p>Photographer Maker Doer</p>
                </div>
                </div>
                <div className="card back">
                <div className="yellow"></div>
                <div className="top dots"></div>
                <div className="personal-info">
                    <p>Krista Stone</p>
                    <p>Photographer. Maker. Doer.</p>
                    <p>123 Address St</p>
                    <p>Sacramento, CA 14234</p>
                    <p>567.890.1234</p>
                    <p>www.kristastone.com</p>
                    <p>@kristastone</p>
                </div>
                <div className="bottom dots"></div>
                <div className="pink"></div>
                </div>
            </div>  
        )
    }
}
export default Card