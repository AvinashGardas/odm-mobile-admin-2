import React, {Component} from 'react'
import firebase from 'firebase'
//ui
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import CircularProgress from 'material-ui/CircularProgress'
import Snackbar from 'material-ui/Snackbar'
import Divider from 'material-ui/Divider'
import Dialog from 'material-ui/Dialog'

class ExperimentAddImageCarouselDataForPath extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: null,

            path: '',
            subPath: '',
            openOverlay: false,
            openSnackbar: false,
            snackbarMessage: ''
        }
    }

    componentDidMount() { 
    }

    handleInput = (event) => {
        const id = event.target.id
        const value = event.target.value

        this.setState({[id]: value})
    }

    openOverlay = () => {
        this.setState({openOverlay: true})
    }

    closeOverlay = () => {
        this.setState({openOverlay: false})
    }

    closeSnackbar = () => {
        this.setState({openSnackbar: false})
    }

    uploadPhotoDataToPath = () => {
        if(this.state.path.slice(-1) === '/') {
            //allow only when / is at the end of Path
            const path = `${this.state.path}${this.state.subPath}`
            const imageCarouselObject = {
                "description" : {
                "backgroundColor" : "rgba(255,255,255,0.9)",
                "color" : "rgba(49, 49, 49, 0.9)",
                "title" : "<span>Ministry</span>"
                },
                "name" : "name",
                "thumbnail" : "https://via.placeholder.com/640x480",
                "url" : "https://via.placeholder.com/640x480"
            }

            const ref = firebase.database().ref(path)
            
            ref.once('value', snapshot => {
                this.setState({data: snapshot.val()})

                if(this.state.data!==null) {
                    //data already exists
                    this.setState({snackbarMessage: 'Data already exists at this path! Please verify and try again.', openSnackbar: true})
                } 
                else if(this.state.data === null) {
                    //upload data
                    ref.set(imageCarouselObject)
                    .then(() => {
                        this.closeOverlay()
                        this.setState({snackbarMessage: 'Data uploaded', openSnackbar: true})
                    })
                    .catch((error) => {
                        this.closeOverlay()
                        this.setState({snackbarMessage: error.message, openSnackbar: true})
                    })
                }
            })
        }
        else if(this.state.path.slice(-1) === '/') {
            this.setState({snackbarMessage: 'Invalid path format!', openSnackbar: true})
            this.closeOverlay()
        }
        
        //reset data
        this.setState({data: null})
    }

    handleAddButton = () => {
        this.openOverlay()
    }

    render() {
        const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={this.closeOverlay}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              onClick={this.uploadPhotoDataToPath}
            />
        ]

        return(
            <div>
                <Divider style={{width: `100%`, marginTop: 8, marginBottom: 8, backgroundColor: '#bdbdbd'}} />
                <pre>For Web Image Carousels</pre>

                <div className="pure-g" style={{marginTop: 8}}>
                    <div className="pure-u-14-24">
                        <div className="pure-g">
                            <div className="pure-u-20-24">
                                <TextField
                                style={{width: `90%`}}
                                id="path"
                                hintText="/web/home/slider/images/1"
                                floatingLabelText="Firebase data path"
                                onChange={this.handleInput} />
                            </div>

                            <div className="pure-u-4-24">
                                <TextField
                                style={{width: `60%`}}
                                id="subPath"
                                hintText="5"
                                floatingLabelText="Index"
                                onChange={this.handleInput} />
                            </div>
                        </div>
                    </div>

                    <div className="pure-u-10-24 display-flex-center">
                        {
                            this.state.path!=='' && this.state.subPath!=='' ?
                            <FlatButton label="Add carousel data to ODM-Development" className="button-text-lower-case" onTouchTap={this.handleAddButton} />
                            :
                            <FlatButton label="Enter valid path and index, to add data carousel data" className="button-text-lower-case-disabled" disabled />
                        }
                        
                    </div>
                </div>

                <Dialog
                title="ODM Development"
                actions={actions}
                modal={true}
                open={this.state.openOverlay}>

                    <div className="pure-g">
                        <div className="pure-u-1">
                            <div className="pure-g">
                                <div className="pure-u-1">
                                    <pre>Do you want to add data here? Double check the path!</pre>
                                </div>
                            </div>
                            <br/>
                            
                            <div className="pure-g">
                                <div className="pure-u-1">
                                    <pre>{this.state.path}{this.state.subPath}</pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </Dialog>

                <Snackbar
                className="snackbar"
                open={this.state.openSnackbar}
                message={this.state.snackbarMessage}
                autoHideDuration={4000}
                onRequestClose={this.closeSnackbar} />
            </div>
        )
    }
}

export default ExperimentAddImageCarouselDataForPath