import React, {Component} from 'react'
import firebase from 'firebase'
//ui
import FlatButton from 'material-ui/FlatButton'
import CopyIcon from 'material-ui/svg-icons/content/content-copy'
import IconButton from 'material-ui/IconButton'
import {CopyToClipboard} from 'react-copy-to-clipboard'

class StorageList extends Component {
    constructor(props) {
        super(props)
        this.limit = 0

        this.state = {
            photos_data: null,
            photos_data_length: 0,
            photos_data_indicator: true,

            thumbnails_data: null
        }
    }
    
    componentDidMount() {
        window.addEventListener('scroll', this.onScroll)

        //album data
        this.limit += 9
        this.fetchMetaData(this.limit)
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll)
    }

    fetchMetaData = (limit) => {
        const metaDataRef = firebase.database().ref('/metadata/photos').limitToFirst(limit)
        metaDataRef.on('value', snapshot => {
            const data = snapshot.val()
            
            if(data !== null) {
                //data
                this.setState({photos_data: data})
                //length
                if(typeof snapshot.val() === 'object') {
                    const length = Object.keys(data).length
                    this.setState({photos_data_length: length})
                    console.log('object', length)
                }
                else if(typeof snapshot.val() === 'array') {
                    const length = Object.keys(data.length)
                    this.setState({photos_data_length: length})
                    console.log('array', length)
                }
            }
            
        })
    }

    onScroll = () => {
        if((window.scrollY) <= (document.getElementsByClassName('admin-storage-list-container')[0].offsetHeight) - 824 - 36
        && this.limit <= this.state.photos_data_length) {
           
            this.setState({photos_data_indicator: true})
            this.loadMoreData()
        }
    }

    loadMoreData = () => {
        //fetch new data
        if(this.limit <= this.state.photos_data_length) {
            this.limit += 9
            this.fetchMetaData(this.limit)
        }
    }

    copyToClipboard = (url) => {
        var textField = document.createElement('textarea')
        textField.style.height = "0px"
        textField.innerText = url
        document.body.appendChild(textField)
        textField.select()
        document.execCommand('copy')
        textField.remove()
    }

    renderList() {
        const data = this.state.photos_data
        
        return(
            <div className="pure-g">
                {
                    Object.values(data)
                    .map((item, key) => {
                        return(
                            <div className="pure-u-1 pure-u-sm-12-24 pure-u-md-8-24 pure-u-xl-6-24" 
                            style={{paddingLeft: 8}}
                            key={key}>

                                <div
                                className="storage-list-media-item" 
                                style={{height: 340, width: 'auto', paddingRight: 8}}>

                                    <a className="storage-list-anchor-item"
                                    href={`${item.downloadURLs}`} 
                                    target="_blank">

                                        <div 
                                        style={{backgroundImage: `url('${item.downloadURLs}')`, height: 240, width: 'auto', borderRadius: 4}}
                                        className="storage-list-image background-image-full">
                                            {/* image url */}
                                        </div>
                                    </a>

                                    <div style={{paddingTop: 10, fontSize: 15, color: '#262626'}}>
                                        <span>{item.name}</span>
                                    </div>

                                    <div className="pure-g" style={{paddingTop: 0}}>
                                        <div className="pure-u-3-24 display-flex-start">
                                            <span style={{backgroundColor: '#e0e0e0', fontSize: 13, color: 'rgb(117, 117, 117)', padding: 4}}>
                                                #{key}
                                            </span>
                                        </div>

                                        <div className="pure-u-6-24 display-flex-start">
                                            <span style={{backgroundColor: '#e0e0e0', fontSize: 13, color: 'rgb(117, 117, 117)', padding: 4}}>
                                                {item.size}
                                            </span>
                                        </div>

                                        <div className="pure-u-15-24 display-flex-start">
                                            <span style={{fontSize: 13, color: 'rgb(117, 117, 117)', padding: 0}}>
                                                <FlatButton 
                                                icon={<CopyIcon className="svg-icon-small" />}
                                                labelPosition="after"
                                                label="copy url" 
                                                className="button-text-lower-case" 
                                                onTouchTap={ () => {this.copyToClipboard(item.downloadURLs)} } />
                                            </span>
                                            
                                        </div>
                                    </div>

                                </div>
                                
                                
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    loadAllPhotos = () => {
        //reset
        this.setState({photos_data: null, photos_data_length: 0})

        const metaDataRef = firebase.database().ref('/metadata/photos')
        metaDataRef.once('value', snapshot => {
            const data = snapshot.val()
            
            if(data !== null) {
                //length
                const length = Object.keys(data).length
                //data
                this.setState({photos_data: data, photos_data_length: length})
                this.limit = length
            }
        })
    }

    render() {
        const photos_data = this.state.photos_data
        
        return(
            <div className="admin-storage-list-container">

                <div className="pure-g" style={{margin: 8}}>
                    <div className="pure-u-12-24 display-flex-start">
                        <label>Photos in storage</label>
                    </div>

                    <div className="pure-u-12-24 display-flex-end">
                        <FlatButton label="Load all photos" 
                        className="button-text-lower-case" 
                        onTouchTap={this.loadAllPhotos} />
                    </div>
                </div>

                {
                    photos_data !== null ?
                    this.renderList()
                    :
                    <div style={{marginLeft: 8, marginRight: 8}}>
                        Loading...
                    </div>
                }

                {
                    (photos_data !== null) && (this.state.photos_data_indicator) && (this.limit <= this.state.photos_data_length) ?
                    <div style={{marginLeft: 8, marginRight: 8}}>
                        Fetching data...
                    </div>
                    :
                    <div></div>
                }
            </div>
        )
    }
}

export default StorageList