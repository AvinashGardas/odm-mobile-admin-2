import React, {Component} from 'react'
import firebase from 'firebase'
//ui
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import CircularProgress from 'material-ui/CircularProgress'
import Snackbar from 'material-ui/Snackbar'
import Divider from 'material-ui/Divider'
import Dialog from 'material-ui/Dialog'

// ODM Mobile App Prod - firebase
const odm_android_app_prod_config = {
    apiKey: "AIzaSyAYx5db0o2dvdJ502k1z3EOFMrUER3ilZ4",
    authDomain: "odm-android-app-eb43e.firebaseapp.com",
    databaseURL: "https://odm-android-app-eb43e.firebaseio.com",
    projectId: "odm-android-app-eb43e",
    storageBucket: "odm-android-app-eb43e.appspot.com",
    messagingSenderId: "306231514599"
}

//firebase app initializations
const odm_android_app_prod_initializeApp = firebase.initializeApp(odm_android_app_prod_config, "odm-prod-storage")

//databases
const odm_android_app_prod_storage = odm_android_app_prod_initializeApp.storage()

class ExperimentAddPhotoMetadata extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: null,
            latestId: 0,

            path: 'media/photos',
            subPath: '',
            openOverlay: false,
            openSnackbar: false,
            snackbarMessage: ''
        }
    }

    componentDidMount() { 
    }

    handleInput = (event) => {
        const id = event.target.id
        const value = event.target.value

        this.setState({[id]: value})
    }

    openOverlay = () => {
        this.setState({openOverlay: true})
    }

    closeOverlay = () => {
        this.setState({openOverlay: false})
    }

    closeSnackbar = () => {
        this.setState({openSnackbar: false})
    }

    uploadMetadata = () => {
        //var storage = firebase.storage()
        var photosStorageRef = odm_android_app_prod_storage.ref(this.state.path)
        var image = photosStorageRef.child(this.state.subPath)

        image.getMetadata()
              .then((data) => {
                  console.log(data)
                  this.setState({data: data})
                  this.addMetadataToDatabase()
                })
                .catch((error) => {
                    this.setState({snackbarMessage: `${error.message}`, openSnackbar: true})
                    this.closeOverlay()
                })
    }

    addMetadataToDatabase = () => {
        // odm development database
        const photosMetadataRef = firebase.database().ref('/metadata/photos')
        photosMetadataRef.once('value', snapshot => {
            const data = snapshot.val()
            let id = 0
            let latestId

            if(data!==null) {
                Object.entries(data)
                        .map(([key]) => {
                            id = key 
                        })

                latestId = parseInt(id)
                latestId += 1
                this.setState({latestId})
                
                //upload metadata
                this.uploadMetadataToDatabase()
            }
            else {
                //no data
                latestId = 0
                this.setState({latestId})
                
                //upload metadata
                this.uploadMetadataToDatabase()
            }
        })
    }

    uploadMetadataToDatabase = () => {
        const metadata = this.state.data
        const photosMetadataRef = firebase.database().ref(`/metadata/photos/${this.state.latestId}`)

        const size = this.formatBytes(metadata.size)
        photosMetadataRef.set({
            type: ''+metadata.type,
            bucket: ''+metadata.bucket,
            generation: ''+metadata.generation,
            metageneration: ''+metadata.metageneration,
            fullPath: ''+metadata.fullPath,
            name: ''+metadata.name,
            size: ''+size,
            timeCreated: ''+metadata.timeCreated,
            updated: ''+metadata.updated,
            md5Hash: ''+metadata.md5Hash,
            cacheControl: ''+metadata.cacheControl,
            contentDisposition: ''+metadata.contentDisposition,
            contentEncoding: ''+metadata.contentEncoding,
            contentLanguage: ''+metadata.contentLanguage,
            contentType: ''+metadata.contentType,
            customMetadata: ''+metadata.customMetadata,
            downloadURLs: ''+metadata.downloadURLs
        })
        
        this.closeOverlay()
        this.setState({snackbarMessage: 'Photo metadata uploaded', openSnackbar: true})
    }

    formatBytes(bytes, decimals) {
        if (bytes === 0)
        return '0 Bytes';

        var k = 1000;
        var dm = decimals + 1 || 2;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        var i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    handleAddButton = () => {
        this.openOverlay()
    }

    render() {
        const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={this.closeOverlay}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              onClick={this.uploadMetadata}
            />
        ]

        return(
            <div>
                <Divider style={{width: `100%`, marginTop: 8, marginBottom: 8, backgroundColor: '#bdbdbd'}} />
                <pre>Add photo metadata to database</pre>

                <div className="pure-g" style={{marginTop: 8}}>
                    <div className="pure-u-14-24">
                        <div className="pure-g">
                            <div className="pure-u-12-24">
                                <TextField
                                style={{width: `80%`}}
                                id="path"
                                hintText="/media/photos"
                                defaultValue="/media/photos"
                                floatingLabelText="Storage path"
                                onChange={this.handleInput} />
                            </div>

                            <div className="pure-u-12-24">
                                <TextField
                                style={{width: `80%`}}
                                id="subPath"
                                hintText="GFJ_2017_1.jpg"
                                floatingLabelText="Photo path"
                                onChange={this.handleInput} />
                            </div>
                        </div>
                    </div>

                    <div className="pure-u-10-24 display-flex-center">
                        {
                            this.state.path!=='' && this.state.subPath!=='' ?
                            <FlatButton label="Add photo metadata to ODM-Development" className="button-text-lower-case" onTouchTap={this.handleAddButton} />
                            :
                            <FlatButton label="Enter valid photo path, to add metadata to database" className="button-text-lower-case-disabled" disabled />
                        }
                        
                    </div>
                </div>

                <Dialog
                title="Photo metadata - ODM Development"
                actions={actions}
                modal={true}
                open={this.state.openOverlay}>

                    <div className="pure-g">
                        <div className="pure-u-1">
                            <div className="pure-g">
                                <div className="pure-u-1">
                                    <pre>Do you want to add metadata here? Double check the path!</pre>
                                </div>
                            </div>
                            <br/>
                            
                            <div className="pure-g">
                                <div className="pure-u-1">
                                    <pre>{this.state.path}{this.state.subPath}</pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </Dialog>

                <Snackbar
                className="snackbar"
                open={this.state.openSnackbar}
                message={this.state.snackbarMessage}
                autoHideDuration={4000}
                onRequestClose={this.closeSnackbar} />
            </div>
        )
    }
}

export default ExperimentAddPhotoMetadata