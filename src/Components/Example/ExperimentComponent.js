import React, {Component} from 'react'
import firebase from 'firebase'
import DropZone from 'react-dropzone'
import axios from 'axios'
//ui
import FlatButton from 'material-ui/FlatButton'
import UploadIcon from 'material-ui/svg-icons/file/file-upload'
import CropImageIcon from 'material-ui/svg-icons/image/crop'
import CircularProgress from 'material-ui/CircularProgress'
import Snackbar from 'material-ui/Snackbar'
import Divider from 'material-ui/Divider'
import Slider from 'material-ui/Slider'

// ODM Mobile App Prod - firebase
const odm_android_app_prod_config = {
    apiKey: "AIzaSyAYx5db0o2dvdJ502k1z3EOFMrUER3ilZ4",
    authDomain: "odm-android-app-eb43e.firebaseapp.com",
    databaseURL: "https://odm-android-app-eb43e.firebaseio.com",
    projectId: "odm-android-app-eb43e",
    storageBucket: "odm-android-app-eb43e.appspot.com",
    messagingSenderId: "306231514599"
}

//firebase app initializations
const odm_android_app_prod_initializeApp = firebase.initializeApp(odm_android_app_prod_config, "odm-android-app-prod-storage")

//databases
const odm_android_app_prod_storage = odm_android_app_prod_initializeApp.storage()

class ExperimentComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showToast: false,
            message: '',

            originalImage: '',
            thumbnailBlob: null,

            photoURL: '',
            photoSize: '',
            thumbnailURL: '',
            thumbnailSize: '',

            taskPhoto: '',
            taskThumbnail: '',
            photoCompleted: 0,
            thumbnailCompleted: 0,

            resizedImageDivHeight: 'auto',
            resizedImageUrl: '',
            resizedImageWidth: 'auto',
            resizedImageHeight: 'auto',
            resizedImageSize: 0,
            percentageSlider: 90
        }
    }

    downscaleImage(dataUrl, newWidth, imageType, imageArguments) {
        "use strict";
        var image, oldWidth, oldHeight, newHeight, canvas, ctx, newDataUrl;
    
        // Provide default values
        imageType = imageType || "image/jpeg";
        imageArguments = imageArguments || 0.7;
    
        // Create a temporary image so that we can compute the height of the downscaled image.
        image = new Image();
        image.src = dataUrl;
        oldWidth = image.width;
        oldHeight = image.height;
        newHeight = Math.floor(oldHeight / oldWidth * newWidth)
    
        // Create a temporary canvas to draw the downscaled image on.
        canvas = document.createElement("canvas");
        canvas.width = newWidth;
        canvas.height = newHeight;
    
        // Draw the downscaled image on the canvas and return the new data URL.
        ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0, newWidth, newHeight);
        newDataUrl = canvas.toDataURL(imageType, imageArguments);
        return newDataUrl;
    }

    resizeImage(dataUrl, imageType, reducePercent) {
        "use strict";
        var image, oldWidth, oldHeight, newHeight, newWidth, canvas, ctx, newDataUrl;
    
        // Provide default values
        imageType = imageType || "image/jpeg";
    
        // Create a temporary image so that we can compute the height of the downscaled image.
        image = new Image();
        image.src = dataUrl;
        oldWidth = image.width;
        oldHeight = image.height;
        //reduction factor
        const reductionFactor = 100/reducePercent
        newHeight = Math.floor(oldHeight / reductionFactor)
        newWidth = Math.floor(oldWidth / reductionFactor)
    
        // Create a temporary canvas to draw the downscaled image on.
        canvas = document.createElement("canvas");
        canvas.width = newWidth;
        canvas.height = newHeight;
    
        // Draw the downscaled image on the canvas and return the new data URL.
        ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0, newWidth, newHeight);
        newDataUrl = canvas.toDataURL(imageType);

        //create object for re-sized image
        const resizedImage = {
            dataUrl: newDataUrl,
            height: newHeight,
            width: newWidth
        }

        return resizedImage
    }

    //choose a photo
    onImageUpload = (event) => {
        //reset
        this.setState({photoCompleted: 0, thumbnailCompleted: 0, resizedImageUrl: '', resizedImageDivHeight: 'auto', resizedImageWidth: 'auto', resizedImageHeight: 'auto', resizedImageSize: 0})

        const preview = document.getElementById('preview')
        const file = event.target.files[0]
        //console.log(file)
        this.setState({originalImage: file, originalImageName: file.name})
        var reader = new FileReader()

        reader.addEventListener("load", ()=>{
            preview.src = reader.result
            this.setState({originalImage: preview.src})
          }, false)

        if(file) {
            reader.readAsDataURL(file)
        }
    }

    //choose a photo - drag and drop
    onDrop = (files) => {
        //reset
        this.setState({originalImage: '', originalImageName: '', photoURL: '', thumbnailURL: '', photoSize: '', thumbnailSize: '', photoCompleted: 0, thumbnailCompleted: 0, resizedImageUrl: '', resizedImageDivHeight: 'auto', resizedImageWidth: 'auto', resizedImageHeight: 'auto', resizedImageSize: 0})
        document.getElementById('resizedImage').src = ""
        //console.log(files[0])

        const preview = document.getElementById('preview')
        const file = files[0]
        this.setState({originalImage: file, originalImageName: file.name})
        var reader = new FileReader()

        reader.addEventListener("load", ()=>{
            preview.src = reader.result
            this.setState({originalImage: preview.src})
          }, false)

        if(file) {
            reader.readAsDataURL(file)
        }
    }

    //scale down image
    scaleDown = () => {
        var res = this.downscaleImage(this.state.originalImage, 400)
        //console.log(res)
        const thumbnail = document.getElementById('thumbnail')
        thumbnail.src = res

        var ImageURL = res
        // Split the base64 string in data and contentType
        var block = ImageURL.split(";");
        // Get the content type
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

        // Convert to blob
        var blob = this.b64toBlob(realData, contentType);
        this.setState({thumbnailBlob: blob})
    }

    //resize image
    reSize = () => {
        var resizeImageObject = this.resizeImage(this.state.originalImage, "image/jpeg", this.state.percentageSlider)
        const url = resizeImageObject.dataUrl
        const width = resizeImageObject.width
        const height = resizeImageObject.height

        const resizedImage = document.getElementById('resizedImage')
        resizedImage.src = url

        var ImageURL = url
        // Split the base64 string in data and contentType
        var block = ImageURL.split(";");
        // Get the content type
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

        // Convert to blob
        var blob = this.b64toBlob(realData, contentType)
        const resizedImageSize = this.formatBytes(blob.size)
        this.setState({resizedImageSize})

        this.setState({resizedImageUrl: url, resizedImageWidth: width, resizedImageHeight: height, resizedImageDivHeight: 350})
    }

    //download resized image
    downloadResizedImage = () => {
        //download image
        if(this.state.resizedImageUrl!=='') {
            this.downloadMediaFile(this.state.resizedImageUrl)
        }
    }

    //UPLOAD PHOTO & THUMBNAIL
    uploadToStorage = () => {
        //storage
        const storage = odm_android_app_prod_storage

        //photo upload
        const photo_name = this.state.originalImageName
        const photoRef = storage.ref(`media/photos/${photo_name}`)
        const taskPhoto = photoRef.putString(this.state.originalImage, 'data_url')
        this.setState({ taskPhoto: taskPhoto })

        taskPhoto.on('state_changed', (snapshot) => {
            //success
            let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            this.setState({photoCompleted: percentage})
            
        }, (error) => {
            //error
            this.setState({message: `Error: ${error.message}`,showToast: true})
        }, () => {
            const photoSize = this.formatBytes(taskPhoto.snapshot.metadata.size)
                //download url
                taskPhoto.snapshot.ref
                .getDownloadURL()
                .then((downloadURL) => {
                    this.setState({photoURL: downloadURL})
                })
            //later
            this.setState({
                message: `photo uploaded`,
                showToast: true,
                photoSize
            })
            //get photo metadata
            this.getPhotoMetadata()

            //upload thumbnail
            const thumbnail_name = `thumbnail-${this.state.originalImageName}`
            const thumbnailRef = storage.ref(`media/photos/${thumbnail_name}`)
            const taskThumbnail = thumbnailRef.put(this.state.thumbnailBlob)
            this.setState({ taskThumbnail: taskThumbnail })
            
            taskThumbnail.on('state_changed', (snapshot) => {
                //success
                let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                this.setState({thumbnailCompleted: percentage})
            }, (error) => {
                //error
                this.setState({message: `Error: ${error.message}`,showToast: true})
            }, () => {
                const thumbnailSize = this.formatBytes(taskThumbnail.snapshot.metadata.size)
                    //download url
                    taskThumbnail.snapshot.ref
                    .getDownloadURL()
                    .then((downloadURL) => {
                        this.setState({thumbnailURL: downloadURL})
                    })
                //later
                this.setState({
                    message: `thumbnail uploaded`,
                    showToast: true,
                    thumbnailURL: taskThumbnail.snapshot.downloadURL,
                    thumbnailSize: thumbnailSize
                })
                //get thumbnail metadata
                this.getThumbnailMetadata()
                
                //reset
                this.setState({
                    originalImage: '',
                    thumbnailBlob: null
                })
            })
            
        })
    }

    getPhotoMetadata = () => {
        var photosStorageRef = odm_android_app_prod_storage.ref('media/photos')
        const imagePath = this.state.originalImageName
        var image = photosStorageRef.child(imagePath)

        image.getMetadata()
              .then((metadata) => {
                  this.addMetadataToDatabase(metadata, '/metadata/photos')
                })
                .catch((error) => {
                    this.setState({snackbarMessage: `${error.message}`, openSnackbar: true})
                })
    }

    getThumbnailMetadata = () => {
        var photosStorageRef = odm_android_app_prod_storage.ref('media/photos')
        const imagePath = this.state.originalImageName
        var image = photosStorageRef.child(imagePath)

        image.getMetadata()
              .then((metadata) => {
                  this.addMetadataToDatabase(metadata, '/metadata/thumbnails')
                })
                .catch((error) => {
                    this.setState({snackbarMessage: `${error.message}`, openSnackbar: true})
                })
    }
    
    addMetadataToDatabase = (metadata, path) => {
        // odm development database
        const photosMetadataRef = firebase.database().ref(path)
        photosMetadataRef.once('value', snapshot => {
            const data = snapshot.val()
            let id = 0
            let latestId

            if(data!==null) {
                Object.entries(data)
                        .map(([key]) => {
                            id = key 
                        })

                latestId = parseInt(id)
                latestId += 1
                
                //upload metadata
                this.uploadMetadataToDatabase(metadata, path, latestId)
            }
            else {
                //no data
                latestId = 0
                
                //upload metadata
                this.uploadMetadataToDatabase(metadata, path, latestId)
            }
        })
    }

    uploadMetadataToDatabase = (metadata, path, latestId) => {
        const photosMetadataRef = firebase.database().ref(`${path}/${latestId}`)

        const size = (path === '/metadata/photos' ? this.state.photoSize : (path === '/metadata/thumbnails' ? this.state.thumbnailSize : ''))
        const downloadURLs = (path === '/metadata/photos' ? this.state.photoURL : (path === '/metadata/thumbnails' ? this.state.thumbnailURL : ''))
        const name = (path === '/metadata/photos' ? this.state.originalImageName : (path === '/metadata/thumbnails' ? `thumbnail-${this.state.originalImageName}` : ''))

        photosMetadataRef.set({
            type: ''+metadata.type,
            bucket: ''+metadata.bucket,
            generation: ''+metadata.generation,
            metageneration: ''+metadata.metageneration,
            fullPath: ''+metadata.fullPath,
            name: name,
            size: ''+size,
            timeCreated: ''+metadata.timeCreated,
            updated: ''+metadata.updated,
            md5Hash: ''+metadata.md5Hash,
            cacheControl: ''+metadata.cacheControl,
            contentDisposition: ''+metadata.contentDisposition,
            contentEncoding: ''+metadata.contentEncoding,
            contentLanguage: ''+metadata.contentLanguage,
            contentType: ''+metadata.contentType,
            customMetadata: ''+metadata.customMetadata,
            downloadURLs: downloadURLs
        })
    }

    formatBytes(bytes, decimals) {
        if (bytes === 0)
          return '0 Bytes';
    
        var k = 1000;
        var dm = decimals + 1 || 2;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        var i = Math.floor(Math.log(bytes) / Math.log(k))
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
    }
    

    b64toBlob = (b64Data, contentType, sliceSize) => {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }

    handleRequestClose = () => {
        this.setState({
          showToast: false,
        })
    }

    downloadMediaFile = (media_url) => {
        axios({
            url: media_url,
            method: 'GET',
            responseType: 'blob'
        }).then((response) => {
            //open snackbar
            console.log('Downloading file...')
            
            const blob = new Blob([response.data], {type: response.data.type})
            
            const download_url = window.URL.createObjectURL(blob)
            const download_link = document.createElement("a")
            download_link.href = download_url
    
            download_link.setAttribute('download', this.state.originalImageName)
            document.body.appendChild(download_link)
            download_link.click()
        }).catch(() => {
            //open snackbar
            console.log('Error while downloading file!')
        })
    }

    handlePercentageSlider = (event, value) => {
        this.setState({percentageSlider: value})
        //resize image
        this.reSize()
    }

    render() {
        return(
            <div className="main-container" style={{marginTop: 150}}>

                <div className="pure-g" style={{marginBottom: 32}}>
                    <div className="pure-u-8-24">
                        {
                            this.state.originalImage !== '' ?
                            <div>
                                <FlatButton label="Resize photo" className="button-text-lower-case" onTouchTap={this.reSize} labelPosition="after" icon={<CropImageIcon/>} />
                                
                                <Slider
                                min={1}
                                max={100}
                                step={1}
                                value={this.state.percentageSlider}
                                onChange={this.handlePercentageSlider}
                                style={{margin: 8}}
                                />

                                <span>Resize photo by: {this.state.percentageSlider}%</span>
                            </div>
                            :
                            <FlatButton label="Upload a photo to resize" className="button-text-lower-case-disabled" disabled />
                        }
                        
                    </div>

                    <div className="pure-u-16-24">
                        <div>

                            <div className="pure-g">
                                <div className="pure-u-1" style={{height: this.state.resizedImageDivHeight}}>
                                    <img id="resizedImage" src="" alt="resizedImage preview..." style={{height: `100%`, width: 'auto', objectFit: 'contain'}} />
                                    <br/>
                                    <div>
                                        {
                                            this.state.resizedImageWidth !== 'auto' ?
                                            <span>{this.state.resizedImageWidth}</span>
                                            :
                                            null
                                        }

                                        {
                                            this.state.resizedImageHeight !== 'auto' ?
                                            <span>x{this.state.resizedImageHeight}</span>
                                            :
                                            null
                                        }
                                            <span style={{marginLeft: 8, marginRight: 8}}></span>
                                        {
                                            this.state.resizedImageSize !== 0 ?
                                            <span>{this.state.resizedImageSize}</span>
                                            :
                                            null
                                        }
                                            <span style={{marginLeft: 8, marginRight: 8}}></span>
                                        {
                                            this.state.resizedImageUrl !== '' ?
                                            <FlatButton label="Download re-sized photo" className="button-text-lower-case" onTouchTap={this.downloadResizedImage} />
                                            :
                                            <FlatButton label="Resize a photo to download" className="button-text-lower-case-disabled" disabled />
                                        }
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <Divider style={{width: `100%`, backgroundColor: '#bdbdbd'}} />

                <div className="pure-g" style={{marginTop: 16}}>
                    <div className="pure-u-1 display-flex">
                        {
                            (this.state.originalImage!=='' && this.state.thumbnailBlob!==null) ?
                                <FlatButton label="Upload to storage" className="button-text-lower-case" onTouchTap={this.uploadToStorage} labelPosition="after" icon={<UploadIcon />} />
                                :
                                <FlatButton label="Choose a photo, generate to upload it!" className="button-text-lower-case-disabled" disabled={true} />
                        }
                        
                    </div>
                </div>
                <br/>

                <div className="pure-g">
                    <div className="pure-u-8-24">
                        {/* <FlatButton
                        className="button-text-lower-case"
                        label="Drag and drop file here (or) Upload a photo"
                        labelPosition="before"
                        style={styles.uploadButton}
                        containerElement="label"
                        >
                            <input type="file" style={styles.uploadInput} onChange={this.onImageUpload} />
                        </FlatButton> */}

                        <span className="button-as-label">Drag and drop file here (or) Upload a photo</span>

                        <DropZone onDrop={this.onDrop}>
                            <div style={{height: `100%`}} className="display-flex-center">
                                <UploadIcon />
                            </div>
                        </DropZone>
                    </div>

                    <div className="pure-u-16-24">
                        <div>
                            <img id="preview" src="" alt="Image preview..." style={{height: 350, width: 'auto', objectFit: 'contain'}} />
                        </div>
                        
                        <pre>{this.state.originalImageName}</pre>

                        <div className="pure-g">
                            <div className="pure-u-1-5 display-flex">
                                <span>status: </span>
                                <span>{Number(this.state.photoCompleted.toFixed(0))}%</span>
                                
                            </div>

                            <div className="pure-u-1-5 display-flex">
                                <CircularProgress
                                    mode="determinate"
                                    value={this.state.photoCompleted}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <br/>

                <div className="pure-g">
                    <div className="pure-u-8-24">
                        {
                            this.state.originalImage!=='' ?
                                <FlatButton label="Generate thumbnail" className="button-text-lower-case" onTouchTap={this.scaleDown} />
                            :
                                <FlatButton label="Choose a photo, generate thumbnail!" className="button-text-lower-case-disabled" disabled />
                        }
                    </div>

                    <div className="pure-u-16-24">
                            <img id="thumbnail" src="" alt="thumbnail preview..." style={{height: 350, width: 'auto', objectFit: 'contain'}} />
                            <pre>
                                {
                                    this.state.originalImage !== '' ?
                                    `thumbnail-${this.state.originalImageName}`
                                    :
                                    null
                                }
                            </pre>

                            <div className="pure-g">
                            <div className="pure-u-1-5 display-flex">
                                <span>status: </span>
                                <span>{Number(this.state.photoCompleted.toFixed(0))}%</span>
                                
                            </div>

                            <div className="pure-u-1-5 display-flex">
                                <CircularProgress
                                    mode="determinate"
                                    value={this.state.photoCompleted}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <br/>

                {
                    this.state.photoURL !== '' ?
                    <div className="pure-g">
                        <div className="pure-u-4-24">
                                <span>Photo URL</span>
                        </div>

                        <div className="pure-u-20-24">
                            <span>{this.state.photoURL}</span>
                            <br/>
                            <span>{this.state.photoSize}</span>
                        </div>
                    </div>
                    :
                    null
                }

                <br/>

                {
                    this.state.thumbnailURL !== '' ?
                    <div className="pure-g">
                        <div className="pure-u-4-24">
                                <span>Thumbnail URL</span>
                        </div>

                        <div className="pure-u-20-24">
                            <span>{this.state.thumbnailURL}</span>
                            <br/>
                            <span>{this.state.thumbnailSize}</span>
                        </div>
                    </div>
                    :
                    null
                }
                
                <Snackbar
                className="snackbar"
                open={this.state.showToast}
                message={this.state.message}
                autoHideDuration={1000}
                onRequestClose={this.handleRequestClose}
                />
            </div>
        )
    }
}

export default ExperimentComponent