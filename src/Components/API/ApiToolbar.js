import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withRouter} from 'react-router-dom'
//ui
import FlatButton from 'material-ui/FlatButton'
import Chip from 'material-ui/Chip'
import BackIcon from 'material-ui/svg-icons/hardware/keyboard-backspace'
import ArrowRightIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-right'
//css
import './css/api.css'

class ApiToolbar extends Component {
    handleAction = (callback) => {
        callback()
    }

    handleBack = (url) => {
        this.props.history.push('')
        this.props.history.push(url)
    }

    renderChips() {
        const paths = this.props.apiPath.split('/')
        //skip arrow for starting and ending elements

        return (
            paths.map((element, key) => {
                return (
                    <div key={key} className='display-flex-center'>
                        <Chip>{element}</Chip>
                        {
                            key === 0 || key === (paths.length - 1) ?
                            ''
                            :
                            <ArrowRightIcon />
                        }
                    </div>
                )
            })
        )
    }

    render() {
        const backButtonVisibility = this.props.backButtonVisibility ? {visibility: 'visible'} : {visibility: 'hidden'}
        const saveButtonVisibility = this.props.saveButtonVisibility ? {visibility: 'visible'} : {visibility: 'hidden'}
        const addButtonVisibility = this.props.addButtonVisibility ? {visibility: 'visible'} : {visibility: 'hidden'}
        const deleteButtonVisibility = this.props.deleteButtonVisibility ? {visibility: 'visible'} : {visibility: 'hidden'}

        return(
            <div className='pure-g' style={{padding: 8}}>
                <div className='pure-u-1 pure-u-lg-4-24 pure-u-md-4-24'>
                    <FlatButton 
                    style={backButtonVisibility}
                    label='Back' 
                    icon={<BackIcon />} 
                    onClick={()=>{this.handleBack(this.props.backURL)}} 
                    />
                </div>

                <div className='pure-u-1 pure-u-lg-14-24 pure-u-md-14-24 display-flex-center'>
                    {this.renderChips()}
                </div>

                <div className='pure-u-1 pure-u-lg-6-24 pure-u-md-6-24 display-flex-end'>
                    <FlatButton 
                    style={addButtonVisibility}
                    label='Add' 
                    onClick={()=>{this.handleAction(this.props.handleAddCallback)}} 
                    />

                    <FlatButton 
                    style={saveButtonVisibility}
                    label='Save' 
                    onClick={()=>{this.handleAction(this.props.handleSaveCallback)}} 
                    />

                    <FlatButton 
                    style={deleteButtonVisibility}
                    label='Delete' 
                    onClick={()=>{this.handleAction(this.props.handleDeleteCallback)}} 
                    />
                </div>
            </div>
        )
    }
}

ApiToolbar.propTypes = {
    apiPath: PropTypes.string,
    backURL: PropTypes.string,
    backButtonVisibility: PropTypes.bool,
    saveButtonVisibility: PropTypes.bool,
    addButtonVisibility: PropTypes.bool,
    deleteButtonVisibility: PropTypes.bool,
    handleSaveCallback: PropTypes.func,
    handleDeleteCallback: PropTypes.func
}

ApiToolbar.defaultProps = {
    apiPath: '',
    backURL: '/api',
    backButtonVisibility: true,
    saveButtonVisibility: false,
    addButtonVisibility: false,
    deleteButtonVisibility: false,
    handleSaveCallback: () => {},
    handleDeleteCallback: () => {}
}

export default withRouter(ApiToolbar)