import React, {Component} from 'react'
import firebase from 'firebase'
import ApiToolbar from '../../../ApiToolbar'
//ui
import CircularProgress from 'material-ui/CircularProgress'
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table'

const DATABASE_PATH = '/data/media/photos/album'

class ApiMediaPhotosAlbum extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: ''
        }
    }

    componentDidMount() {
        this.fetchData(DATABASE_PATH)
    }

    fetchData(path) {
        const mediaRef = firebase.database().ref(path)
        mediaRef.once('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    renderList() {
        const data = this.state.data

        return(
            <Table
                selectable={false}
            >
                <TableHeader
                    displaySelectAll={false}
                    adjustForCheckbox={false}
                >
                    <TableRow>
                        <TableHeaderColumn>Title</TableHeaderColumn>
                        <TableHeaderColumn></TableHeaderColumn>
                        <TableHeaderColumn></TableHeaderColumn>
                        <TableHeaderColumn>Action</TableHeaderColumn>
                    </TableRow>
                </TableHeader>

                <TableBody
                    displayRowCheckbox={false}
                >
                {
                    data !== '' ?
                    Object.entries(data).map((element,key) => {
                        return(
                            <TableRow key={key}>
                                <TableRowColumn>
                                    <span>{element[0]} - {element[1].title}</span>
                                </TableRowColumn>

                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>

                                <TableRowColumn>
                                    <a href={`/api/media/photos/album/${element[0]}`}>
                                        Open
                                    </a>
                                </TableRowColumn>
                            </TableRow>
                        )
                    })
                    :
                    <TableRow>
                        <TableRowColumn style={{textAlign: 'center'}}>
                            <CircularProgress />
                        </TableRowColumn>
                    </TableRow>
                }
                </TableBody>
            </Table>
        )
    }

    render() {
        const apiPath = `${DATABASE_PATH}`
        const backURL = `api/media/photos`

        return(
            <div>
                {/* toolbar */}
                <ApiToolbar apiPath={apiPath} backURL={backURL} saveButtonVisibility={false} deleteButtonVisibility={false} />

                {this.renderList()}
            </div>
        )
    }
}
export default ApiMediaPhotosAlbum