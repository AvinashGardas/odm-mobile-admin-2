import React, {Component} from 'react'
import firebase from 'firebase'
//ui
import CircularProgress from 'material-ui/CircularProgress'
import ApiToolbar from '../../../../ApiToolbar'

const DATABASE_PATH = '/data/media/photos/album'

class ApiMediaPhotosAlbumItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: ''
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id
        this.fetchData(`${DATABASE_PATH}/${id}`)
    }

    fetchData(path) {
        const mediaRef = firebase.database().ref(path)
        mediaRef.once('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    renderList() {
        const data = this.state.data
        const id = this.props.match.params.id
        //Object.entries(data) : element[0] - key, element[1] - value

        return(
            data !== '' ?
            Object.entries(data).map((element,key) => {
                if(typeof element[1] !== 'object') {
                    return(
                        <div key={key}>
                            <a href={`/api/media/photos/album/${id}/${element[0]}`}>{element[0]}</a>
                            <span> - {element[1]}</span>
                        </div>
                    )
                } else if(typeof element[1] === 'object') {
                    return(
                        <div key={key}>
                            <a href={`/api/media/photos/album/${id}/photos`}>{element[0]}</a>
                        </div>
                    )
                }
            })
            :
            <CircularProgress />
        )
    }

    render() {
        const id = this.props.match.params.id
        const apiPath = `${DATABASE_PATH}/${id}`
        const backURL = `api/media/photos/album`

        return(
            <div>
                {/* toolbar */}
                <ApiToolbar apiPath={apiPath} backURL={backURL} />

                {this.renderList()}
            </div>
        )
    }
}
export default ApiMediaPhotosAlbumItem