import React, {Component} from 'react'
import firebase from 'firebase'
//ui
import CircularProgress from 'material-ui/CircularProgress'
import Snackbar from 'material-ui/Snackbar'
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table'
import TextField from 'material-ui/TextField'
//components
import ApiToolbar from './../../../../../../ApiToolbar'
import { RaisedButton } from 'material-ui'
import Dialog from 'material-ui/Dialog'

const DATABASE_PATH = '/data/media/photos/album'

const MediaForm = (props) => {
    const formData = props.data

    return(
        <div>
            <Table
                selectable={false}
            >
                <TableBody
                displayRowCheckbox={false}
                >
                    <TableRow>
                        <TableRowColumn>Date</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='date' defaultValue={formData.date} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>Description</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='description' defaultValue={formData.description} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        {/* type - NUMBER */}
                        <TableRowColumn>id</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='id' defaultValue={formData.id} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        {/* type - DOUBLE */}
                        <TableRowColumn>latitude</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='latitude' defaultValue={formData.latitude} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>location</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='location' defaultValue={formData.location} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        {/* type - DOUBLE */}
                        <TableRowColumn>longitude</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='longitude' defaultValue={formData.longitude} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>media_size</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='media_size' defaultValue={formData.media_size} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>media_type</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='media_type' defaultValue={formData.media_type} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>media_url</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='media_url' defaultValue={formData.media_url} onChange={props.handleInput}/>
                            <br />
                            <img id="media-url-preview" crossOrigin="Anonymous" src={formData.media_url} alt={formData.title} style={{height: 300, width: 300, objectFit: 'contain'}}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>thumbnail</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='thumbnail' defaultValue={formData.thumbnail} onChange={props.handleInput}/>
                            <br />

                            <img id="thumbnail-preview" src={formData.thumbnail} alt={formData.title} style={{height: 300, width: 300, objectFit: 'contain'}}/>
                            <br />

                            <a download={`${formData.title}.jpg`} href={formData.media_url} title={formData.title}>
                                1. Save image locally
                            </a>
                            <br />

                            2. Upload image <input type='file' onChange={props.onImageUploadLocal} />
                            <br />
                            <img id="preview" alt="preview" style={{height: 300, width: 300, objectFit: 'contain'}}/>
                            <br />

                            <RaisedButton label="3. Generate, Upload thumbnail" onTouchTap={props.scaleDownAndUpload} disabled={formData.thumbnail !== '' ? true : false}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>title</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='title' defaultValue={formData.title} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>track_length</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='track_length' defaultValue={formData.track_length} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>

                    <TableRow>
                        <TableRowColumn>uploaded_on</TableRowColumn>
                        <TableRowColumn>
                            <TextField id='uploaded_on' defaultValue={formData.uploaded_on} onChange={props.handleInput}/>
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        </div>
    )
}

class ApiMediaPhotosAlbumItem_PhotosDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: '',
            openSnackbar: false,
            snackbarMessage: '',
            openOverlay: false,
            originalImage: '',
            originalImageName: '',

            date: '',
            description: '',
            id: 0,
            latitude: 20.001,
            location: '',
            longitude: 10.122,
            media_size: '',
            media_url: '',
            media_type: '',
            thumbnail: '',
            title: '',
            track_length: '',
            uploaded_on: ''
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id
        const photoid = this.props.match.params.photoid

        this.fetchData(`${DATABASE_PATH}/${id}/photos/${photoid}`)
    }

    //image upload

    //thumbnail generation
    onImageUploadLocal = (event) => {
        const preview = document.getElementById('preview')
        const file = event.target.files[0]
        //console.log(event.target.files[0].name)
        this.setState({originalImage: file, originalImageName: event.target.files[0].name})
        var reader = new FileReader()

        reader.addEventListener("load", ()=>{
            preview.src = reader.result
            this.setState({originalImage: preview.src})
            //console.log(preview.src)
          }, false)

        if(file) {
            reader.readAsDataURL(file)
        }
    }
    
    scaleDownAndUpload = () => {
        var originalImage = this.state.originalImage
        if(originalImage !== '') {
            //open overlay
            this.setState({openOverlay: true})

            var res = this.downscaleImage(originalImage, 400)
            const thumbnail = document.getElementById('thumbnail-preview')
            thumbnail.src = res

            var ImageURL = res
            // Split the base64 string in data and contentType
            var block = ImageURL.split(";");
            // Get the content type
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

            // Convert to blob
            var blob = this.b64toBlob(realData, contentType);
            //console.log(blob.size)

            var storage = firebase.storage().ref(`/media/photos/thumbnail-${this.state.originalImageName}`)
            storage.put(blob)
                    .then((res)=>{
                        //get downloadURL and update thumbnail url in database
                        const thumbnailURL = res.metadata.downloadURLs[0]
                        this.updateThumbnailURL(thumbnailURL)
                    })
                    .catch(error => {
                        console.log('error')
                        this.setState({openOverlay: false})
                    })
        }
    }

    downscaleImage(dataUrl, newWidth, imageType, imageArguments) {
        "use strict";
        var image, oldWidth, oldHeight, newHeight, canvas, ctx, newDataUrl;
    
        // Provide default values
        imageType = imageType || "image/jpeg";
        imageArguments = imageArguments || 0.7;
    
        // Create a temporary image so that we can compute the height of the downscaled image.
        image = new Image();
        image.src = dataUrl;
        oldWidth = image.width;
        oldHeight = image.height;
        newHeight = Math.floor(oldHeight / oldWidth * newWidth)
    
        // Create a temporary canvas to draw the downscaled image on.
        canvas = document.createElement("canvas");
        canvas.width = newWidth;
        canvas.height = newHeight;
    
        // Draw the downscaled image on the canvas and return the new data URL.
        ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0, newWidth, newHeight);
        newDataUrl = canvas.toDataURL(imageType, imageArguments);
        return newDataUrl;
    }

    b64toBlob = (b64Data, contentType, sliceSize) => {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }

    //update thumbnail url
    updateThumbnailURL = (url) => {
        const id = this.props.match.params.id
        const photoid = this.props.match.params.photoid
        const path = `${DATABASE_PATH}/${id}/photos/${photoid}`

        const thumbnailRef = firebase.database().ref(path)
        thumbnailRef.update({
            thumbnail: url
        }).then(()=>{
            this.setState({openOverlay: false, snackbarMessage: 'Thumbnail uploaded', openSnackbar: true})
        }).catch(error => {
            this.setState({openOverlay: false, snackbarMessage: 'Error updating thumbnail', openSnackbar: true})
        })
    }

    fetchData(path) {
        const mediaRef = firebase.database().ref(path)
        mediaRef.once('value', snapshot => {
            this.setState({data: snapshot.val()})
            this.updateInitialState(snapshot.val())
        })
    }

    updateInitialState = (data) => {
        const {date, description, id, latitude, location, longitude, media_size, media_type, media_url, thumbnail, title, track_length, uploaded_on} = data
        this.setState({date, description, id, latitude, location, longitude, media_size, media_type, media_url, thumbnail, title, track_length, uploaded_on})
    }

    handleInput = (event) => {
        const id = event.target.id
        const value = event.target.value

        this.setState({
            [id]: value
        })
        switch(id) {
            case 'id': const id_typeNumber = parseInt(value); this.setState({id: id_typeNumber}); break;
            case 'latitude': const latitude_typeFloat = parseFloat(value); this.setState({latitude: latitude_typeFloat}); break;
            case 'longitude': const longitude_typeFloat = parseFloat(value); this.setState({longitude: longitude_typeFloat}); break;
            default: break;
        }
    }

    handleSave = () => {
        const _id = this.props.match.params.id
        const photoid = this.props.match.params.photoid
        const {date, description, id, latitude, location, longitude, media_size, media_type, media_url, thumbnail, title, track_length, uploaded_on} = this.state
        const row = {
            date,
            description,
            id,
            latitude,
            location,
            longitude,
            media_size, 
            media_type,
            media_url,
            thumbnail,
            title,
            track_length,
            uploaded_on
        }

        const photoRef = firebase.database().ref(`${DATABASE_PATH}/${_id}/photos/${photoid}`)
        photoRef.set(row)
                .then( () => {
                    this.setState({snackbarMessage: 'Updated successfully', openSnackbar: true})
                })
                .catch(error => {
                    this.setState({snackbarMessage: 'Error in updating', openSnackbar: true})
                })
    }

    handleRequestClose = () => {
        this.setState({
          openSnackbar: false,
        })
    }

    renderUI() {
        const data = this.state.data
        
        return(
            data !== '' ?
            <MediaForm 
            data={data}
            handleInput={this.handleInput}
            scaleDownAndUpload={this.scaleDownAndUpload}
            onImageUploadLocal={this.onImageUploadLocal}
            />
            :
            <CircularProgress />
        )
    }

    render() {
        const id = this.props.match.params.id
        const photoid = this.props.match.params.photoid
        const apiPath = `${DATABASE_PATH}/${id}/photos/${photoid}`
        const backURL = `api/media/photos/album/${id}/photos`

        return (
            <div>
                {/* toolbar */}
                <ApiToolbar handleSaveCallback={this.handleSave} apiPath={apiPath} backURL={backURL} saveButtonVisibility={true} deleteButtonVisibility={true} />

                {/* UI */}
                {this.renderUI()}

                <Snackbar
                open={this.state.openSnackbar}
                message={this.state.snackbarMessage}
                autoHideDuration={3000}
                onRequestClose={this.handleRequestClose}
                />

                <Dialog
                title="Please wait"
                modal={true}
                open={this.state.openOverlay}
                >
                    Processing...
                </Dialog>
            </div>
        )
    }
}
export default ApiMediaPhotosAlbumItem_PhotosDetail