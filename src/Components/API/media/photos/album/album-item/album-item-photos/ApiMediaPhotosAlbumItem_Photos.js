import React, {Component} from 'react'
import firebase from 'firebase'
//ui
import CircularProgress from 'material-ui/CircularProgress'
import {GridList, GridTile} from 'material-ui/GridList'
import IconButton from 'material-ui/IconButton'
//constants
import {calcuteTimeForFeedFromNow} from './../../../../../../../Utils/functions'
import ApiToolbar from '../../../../../ApiToolbar'

const DATABASE_PATH = '/data/media/photos/album'
const styles = {
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      height: '100vh'
    },
    gridList: {
      width: 500,
    },
    image : {
        height: '100%',
        width: '100%',
        objectFit: 'cover'
    }
}

class ApiMediaPhotosAlbumItem_Photos extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: ''
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id
        this.fetchData(`${DATABASE_PATH}/${id}/photos`)
    }

    fetchData(path) {
        const mediaRef = firebase.database().ref(path)
        mediaRef.once('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    renderList() {
        const data = this.state.data
        const id = this.props.match.params.id

        return(
            data !== '' ?
            Object.entries(data).map((element,key) => {
                const timestamp = calcuteTimeForFeedFromNow(element[1].date)

                return(
                    <GridTile 
                    key={key}
                    title={element[1].title}
                    subtitle={<span><i className="far fa-clock"></i> {timestamp}</span>}
                    >
                        <a href={`/api/media/photos/album/${id}/photos/${element[0]}`}>
                            <img style={styles.image} src={element[1].media_url} alt='media-photos-album-image' />
                        </a>
                    </GridTile>
                )
            })
            :
            <CircularProgress />
        )
    }

    render() {
        const id = this.props.match.params.id
        const apiPath = `${DATABASE_PATH}/${id}/photos`
        const backURL = `api/media/photos/album/${id}`

        return (
            <div>
                {/* toolbar */}
                <ApiToolbar apiPath={apiPath} backURL={backURL} />

                <div style={styles.root}>
                    <GridList
                    cellHeight={180}
                    style={styles.gridList}
                    >
                        {this.renderList()}
                    </GridList>
                </div>
            </div>
        )
    }
}
export default ApiMediaPhotosAlbumItem_Photos