import React, {Component} from 'react'
import firebase from 'firebase'
import ApiToolbar from '../ApiToolbar'
//ui
import CircularProgress from 'material-ui/CircularProgress'

const DATABASE_PATH = '/data/media'

class ApiMedia extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: ''
        }
    }

    componentDidMount() {
        this.fetchData(DATABASE_PATH)
    }

    fetchData(path) {
        const mediaRef = firebase.database().ref(path)
        mediaRef.once('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    renderList() {
        const data = this.state.data

        return(
            data !== '' ?
            Object.keys(data).map((element,key) => {
                return(
                    <div key={key}>
                        <a href={`/api/media/${element}`}>{element}</a>
                    </div>
                )
            })
            :
            <CircularProgress />
        )
    }

    render() {
        const apiPath = `${DATABASE_PATH}`
        const backURL = `api`

        return(
            <div>
                {/* toolbar */}
                <ApiToolbar apiPath={apiPath} backURL={backURL} />

                {this.renderList()}
            </div>
        )
    }
}
export default ApiMedia