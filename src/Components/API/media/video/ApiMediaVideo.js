import React, {Component} from 'react'
import firebase from 'firebase'

const DATABASE_PATH = '/data/media'

class ApiMediaVideo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: ''
        }
    }

    componentDidMount() {
        this.fetchData(DATABASE_PATH)
    }

    fetchData(path) {
        const mediaRef = firebase.database().ref(path)
        mediaRef.once('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    renderList() {
        const data = this.state.data

        return(
            Object.keys(data).map((element,key) => {
                return(
                    <div key={key}>
                        
                    </div>
                )
            })
        )
    }

    render() {
        return(
            <div>
                {this.renderList()}
            </div>
        )
    }
}
export default ApiMediaVideo