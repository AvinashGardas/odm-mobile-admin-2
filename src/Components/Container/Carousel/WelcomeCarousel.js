import React, {Component} from 'react'
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
  } from 'reactstrap'
  //css
  import 'bootstrap/dist/css/bootstrap.min.css'
  import './css/carousel.css'

  const items = [
    {
      src: 'https://images.unsplash.com/photo-1535598745644-bc7913bb1a2a?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9&s=d004e4b06ad0aa33b9d1b5d09bcfc4cf',
      altText: 'Manage data',
      caption: 'Admin helps you to handle create, read, update, delete operations on database'
    },
    {
      src: 'https://images.unsplash.com/photo-1507238691740-187a5b1d37b8?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9&s=d55a204ce337eb3e72467a667d02d49d',
      altText: 'Monitor changes',
      caption: 'You can adeptly monitor changes made to database at ease'
    },
    {
      src: 'https://images.unsplash.com/photo-1522542550221-31fd19575a2d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9&s=d6793dc9f51066a83935189892bfa041',
      altText: 'Maintain website',
      caption: 'It act\'s like a catalyst for project\'s maintenance'
    }
  ]

  class WelcomeCarousel extends Component {
    constructor(props) {
      super(props);
      this.state = { activeIndex: 0 };
      this.next = this.next.bind(this);
      this.previous = this.previous.bind(this);
      this.goToIndex = this.goToIndex.bind(this);
      this.onExiting = this.onExiting.bind(this);
      this.onExited = this.onExited.bind(this);
    }
  
    onExiting() {
      this.animating = true;
    }
  
    onExited() {
      this.animating = false;
    }
  
    next() {
      if (this.animating) return;
      const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
      this.setState({ activeIndex: nextIndex });
    }
  
    previous() {
      if (this.animating) return;
      const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
      this.setState({ activeIndex: nextIndex });
    }
  
    goToIndex(newIndex) {
      if (this.animating) return;
      this.setState({ activeIndex: newIndex });
    }
  
    render() {
      const { activeIndex } = this.state
      const {componentOpacity, display} = this.props
  
      const slides = items.map((item) => {
        return (
          <CarouselItem
            onExiting={this.onExiting}
            onExited={this.onExited}
            key={item.src}
          >
            <img src={item.src} alt={item.altText} className="carousel-image" />
            <CarouselCaption captionText={item.caption} captionHeader={item.altText} />

            {/* Circular descriptionn
            <div className="display-flex-center carousel-description-container">
              <h4>{item.caption}</h4>
            </div> */}
          </CarouselItem>
        );
      });
  
      return (
        <Carousel
          className="carousel-container"
          activeIndex={activeIndex}
          next={this.next}
          previous={this.previous}
          style={{opacity: componentOpacity, transition: '.3s ease', height: `100%`}}>

          <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
          {slides}
          <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
          <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
          
        </Carousel>
      );
    }
  }

export default WelcomeCarousel