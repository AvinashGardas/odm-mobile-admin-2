import React, {Component} from 'react'
import {withRouter, Link} from 'react-router-dom'
//ui
import RaisedButton from 'material-ui/RaisedButton'

class AccessDenied extends Component {
    redirectTo = (url) => {
        switch(url) {
            case 'signin': this.props.history.push('/signin'); break;
            case 'signup': this.props.history.push('/signup'); break;
            default: break;
        }
    }

    render() {
        return(
            <div className="page-container">
                <div style={{backgroundColor: '#f44336', padding: 8, fontSize: 15, color: '#ffffff', borderRadius: 2}} className='roboto-sans display-flex-center'>Access Denied</div>
                <p className="roboto-mono display-flex-center"><Link to='/signin'>Click here</Link>, to sign in.</p>
            </div>
        )
    }
}

export default withRouter(AccessDenied)