import React, {Component} from 'react'
//components
import DrawerMenuList from './DrawerMenuList'
//constants
import Constants from './../../../Utils/Constants'


class SideNav extends Component {
    render() {
        const sideNavDisplayStyle = this.props.visibility ? 'block' : 'none'
        
        return(
            <div id="side-nav" style={{width: Constants.SIDENAV_DEFFAULT_WIDTH, display: sideNavDisplayStyle}}>
                <DrawerMenuList
                handleClose={this.props.handleClose} 
                handleToggle={this.props.handleToggle} 
                sideNavOpen={this.props.sideNavOpen}/>
            </div>
        )
    }
}
export default SideNav