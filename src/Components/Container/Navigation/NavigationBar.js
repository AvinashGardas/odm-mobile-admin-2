import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import firebase from 'firebase'
//components
import SideNav from './SideNav'
//ui
import AppBar from 'material-ui/AppBar'
import * as Colors from 'material-ui/styles/colors'
import IconButton from 'material-ui/IconButton'
import MenuIcon from 'material-ui/svg-icons/navigation/menu'
import FlatButton from 'material-ui/FlatButton'
//constants
import Constants from './../../../Utils/Constants'
import * as firebaseAuth from './../../Firebase/FirebaseAuthenticationApi'

const AuthenticatedAppBar = (props) => {
    const {styles, handleHomeTouch, headerMenuIconStyle, handleToggle, menuIconVisibility, user} = props

    return(
        <AppBar
            id="app-bar"
            className="authenticated-app-bar"
            style={styles.bgColor}
            title={<span style={styles.title}>{Constants.APP_NAME}</span>}
            onTitleTouchTap={handleHomeTouch}
            iconElementLeft={
                menuIconVisibility ?
                <IconButton 
                id="menu-icon" 
                className={headerMenuIconStyle} 
                onClick={handleToggle}> 
                    <MenuIcon color={Colors.white}/> 
                </IconButton>
                :
                <span></span>
            }
            iconElementRight={
                user !== null ?
                <span style={{color: Colors.white}}>{user.email}</span>
                :
                null
            }
            />
    )
}

const UnAuthenticatedAppBar = (props) => {
    const {styles, handleHomeTouch, handleRedirect} = props

    return(
        <AppBar
            style={styles.bgColor}
            title={<span style={styles.title}>{Constants.APP_NAME}</span>}
            onTitleTouchTap={handleHomeTouch}
            iconElementLeft={<p></p>}
            iconElementRight={<AuthenticationButtons style={{marginTop: 0}} handleRedirect={handleRedirect} />}
            />
    )
}

const AuthenticationButtons = (props) => {
    return(
        <div className="display-flex-center" style={{height: `85%`, marginBottom: 8}}>
            <FlatButton label="Signin" style={{color: Colors.white}} onTouchTap={()=>{props.handleRedirect('signin')}} />
            <FlatButton label="Signup" style={{color: Colors.white}} onTouchTap={()=>{props.handleRedirect('signup')}} />
        </div>
    )
}

class NavigationBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            sideNavOpen: false,
            headerMenuIconStyle: "",
            sideNavVisibility: false,
            menuIconVisibility: false
        }
    }

    componentWillMount() {
        //Persisting Login Across Refresh
        this.persistFirebaseLogin()
    }

    componentDidMount() {
        this.updateUIBasedOnLocation(this.props.location.pathname)
    }

    componentWillReceiveProps(nextProps) {
        this.updateUIBasedOnLocation(nextProps.location.pathname)
    }

    updateUIBasedOnLocation = (url) => {
        if( (url === '/' && this.state.user === null) || url === '/signin' || url === '/signup') {
            this.setState({sideNavVisibility: false, menuIconVisibility: false})
        } else {
            this.setState({sideNavVisibility: true, menuIconVisibility: true})
        }
    }

    persistFirebaseLogin = () => {
        firebase.auth().onAuthStateChanged((user) => {
            if(user) {
                this.setState({user})
            }
        })
    }

    handleHomeTouch = () => {
        if(this.state.user === null) {
            //user is not logged in, so redirect to welcome screen with signin options
            this.props.history.push('/')
        } else {
            //user is logged in, so redirect to dashboard
            this.props.history.push('/dashboard')
        }
    }

    handleRedirect = (url) => {
        switch(url) {
            case 'signin': this.props.history.push('/signin'); break;
            case 'signup': this.props.history.push('/signup'); break;
            default: break;
        }
    }

    handleToggle = () => {
        const DefaultMainContainerMarginLeft = Constants.SIDENAV_DEFFAULT_WIDTH + 8;
        const OpenMainContainerMarginLeft = Constants.SIDENAV_OPEN_WIDTH + 8

        this.setState({sideNavOpen: !this.state.sideNavOpen});

        if(!this.state.sideNavOpen) {
            document.getElementById("side-nav").style.width = `${Constants.SIDENAV_OPEN_WIDTH}px`;
            document.getElementById("menu-container").style.marginLeft = `${Constants.SIDENAV_OPEN_WIDTH}px`;
            document.getElementById("main-container").style.marginLeft = `${OpenMainContainerMarginLeft}px`;
            // document.getElementById("page-container").style.marginLeft = `${OpenMainContainerMarginLeft}px`;
            this.setState({headerMenuIconStyle: "menu-icon-hide"});
        } else {
            document.getElementById("side-nav").style.width = `${Constants.SIDENAV_DEFFAULT_WIDTH}px`;
            document.getElementById("menu-container").style.marginLeft = "0px";
            document.getElementById("main-container").style.marginLeft = `${DefaultMainContainerMarginLeft}px`;
            // document.getElementById("page-container").style.marginLeft = `70px`;
            this.setState({headerMenuIconStyle: ""});
        }
        
    }

    handleClose = (menuItem) => {
        switch(menuItem) {
            case Constants.DASHBOARD_URL: this.changeURL(Constants.DASHBOARD_URL)
                                            break

            case Constants.MEDIA_URL: this.changeURL(Constants.MEDIA_URL)
                                        break

            case Constants.FEED_URL: this.changeURL(Constants.FEED_URL)
                                        break

            case Constants.EVENTS_URL: this.changeURL(Constants.EVENTS_URL)
                                        break

            case Constants.ABOUTUS_URL: this.changeURL(Constants.ABOUTUS_URL)
                                        break

            case Constants.NOTIFICATIONS_URL: this.changeURL(Constants.NOTIFICATIONS_URL)
                                                break

            case Constants.INBOX_URL: this.changeURL(Constants.INBOX_URL)
                                        break

            case Constants.BACKUPS_URL: this.changeURL(Constants.BACKUPS_URL)
                                        break

            case Constants.API_URL: this.changeURL(Constants.API_URL)
                                    break
            
            case 'signout': this.handleSignOut()
                            break

            default: break
        }
    }

    changeURL(url) {
        this.props.history.push(`/${url}`)
    }

    handleSignOut = () => {
        firebaseAuth.doSignOut()
                    .then( () => {
                            //remove session
                            sessionStorage.setItem('currentLoggedInEmail', '')
                            //go to / (root)
                            this.props.history.push('/')
                            //reload
                            window.location.reload()
                        })
    }
    
    render() {
        const styles = {
            title: {
              cursor: 'pointer',
              color: Colors.white
            },
            bgColor: {
                backgroundColor: Colors.grey900,
            }
          }
        const {user} = this.state

        return(
            <div>
                {
                    user === null
                    ? <UnAuthenticatedAppBar handleHomeTouch={this.handleHomeTouch} styles={styles} handleRedirect={this.handleRedirect} />
                    : <AuthenticatedAppBar handleHomeTouch={this.handleHomeTouch} styles={styles} headerMenuIconStyle={this.state.headerMenuIconStyle} handleToggle={this.handleToggle} menuIconVisibility={this.state.menuIconVisibility} user={this.state.user} />
                }
                

                {
                    user === null
                    ? null
                    : <SideNav handleToggle={this.handleToggle} handleClose={this.handleClose} sideNavOpen={this.state.sideNavOpen} visibility={this.state.sideNavVisibility}/>
                }
                
            </div>
        )
    }
}
export default withRouter(NavigationBar)