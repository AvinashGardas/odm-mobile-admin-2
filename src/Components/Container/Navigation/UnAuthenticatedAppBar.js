import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import Constants from './../../../Utils/Constants'
import * as Colors from 'material-ui/styles/colors'
import AppBar from 'material-ui/AppBar'
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar'
import Typography from 'material-ui/styles/typography'
import FlatButton from 'material-ui/FlatButton'

const AuthenticationButtons = (props) => {
    return(
        <div>
            <FlatButton label="Signin" backgroundColor={Colors.blue500} onTouchTap={()=>{props.handleRedirect('signin')}} />
            <FlatButton label="Signup" onTouchTap={()=>{props.handleRedirect('signup')}} />
        </div>
    )
}

class UnAuthenticatedAppBar extends Component {
    handleRedirect = (url) => {
        switch(url) {
            case 'signin': this.props.history.push('/signin'); break;
            case 'signup': this.props.history.push('/signup'); break;
            default: break;
        }
    }

    handleHomeTouch = () => {
        this.props.history.push('/')
    }

    render() {
        const styles = {
            title: {
              cursor: 'pointer',
              color: Colors.white
            },
            bgColor: {
                backgroundColor: Colors.blue500,
            }
          }
    
        return(
            <Toolbar style={{backgroundColor: Colors.blue500}}>
                <ToolbarGroup>
                    <ToolbarTitle text={<span style={styles.title}>{Constants.APP_NAME}</span>} />
                </ToolbarGroup>

                <ToolbarGroup>
                    <AuthenticationButtons handleRedirect={this.handleRedirect}/>
                </ToolbarGroup>
            </Toolbar>
        )
    }
}

export default withRouter(UnAuthenticatedAppBar)