import React, {Component} from 'react';
//components
import DrawerMenuList from './DrawerMenuList';
//constants
import Constants from './../../Utils/Constants';


class SideNav extends Component {
    render() {
        return(
            <div id="side-nav" style={{width: Constants.SIDENAV_DEFFAULT_WIDTH}}>
                <DrawerMenuList
                handleClose={this.props.handleClose} 
                handleToggle={this.props.handleToggle} 
                sideNavOpen={this.props.sideNavOpen}/>
            </div>
        )
    }
}
export default SideNav;