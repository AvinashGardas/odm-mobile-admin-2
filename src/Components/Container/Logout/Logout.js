import React,{Component} from 'react';
import firebase from 'firebase'
import {withRouter} from 'react-router-dom';
//functions
import * as firebaseAuth from './../../Firebase/FirebaseAuthenticationApi'
//ui
import RaisedButton from 'material-ui/RaisedButton';

class Logout extends Component {
    handleSignOut = () => {
        firebaseAuth.doSignOut().then(res => {this.props.history.push('/')}).catch(error => {console.log(error)})
    }

    render() {
        return(
            <div>
                <RaisedButton 
                label="Logout"
                primary={true}
                onTouchTap={this.handleSignOut}/>
            </div>
        )
    }
}
export default withRouter(Logout);