import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
//components
import SideNav from './SideNav';
//ui
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import * as Colors from 'material-ui/styles/colors';
//constants
import Constants from './../../Utils/Constants';
//css
import './css/navbar.css';

class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sideNavOpen: false,
            headerMenuIconStyle: ""
        }
    }

    handleHomeTouch = () => {
        this.props.history.push('/');
    }

    handleToggle = () => {
        const DefaultMainContainerMarginLeft = Constants.SIDENAV_DEFFAULT_WIDTH + 8;
        const OpenMainContainerMarginLeft = Constants.SIDENAV_OPEN_WIDTH + 8;

        this.setState({sideNavOpen: !this.state.sideNavOpen});

        if(!this.state.sideNavOpen) {
            document.getElementById("side-nav").style.width = `${Constants.SIDENAV_OPEN_WIDTH}px`;
            document.getElementById("menu-container").style.marginLeft = `${Constants.SIDENAV_OPEN_WIDTH}px`;
            document.getElementById("main-container").style.marginLeft = `${OpenMainContainerMarginLeft}px`;
            this.setState({headerMenuIconStyle: "menu-icon-hide"});
        } else {
            document.getElementById("side-nav").style.width = `${Constants.SIDENAV_DEFFAULT_WIDTH}px`;
            document.getElementById("menu-container").style.marginLeft = "0px";
            document.getElementById("main-container").style.marginLeft = `${DefaultMainContainerMarginLeft}px`;
            this.setState({headerMenuIconStyle: ""});
        }
        
    }

    handleClose = (menuItem) => {
        console.log(menuItem)
       
        switch(menuItem) {
            case Constants.DASHBOARD_URL: this.changeURL(Constants.DASHBOARD_URL); break;
            case Constants.MEDIA_URL: this.changeURL(Constants.MEDIA_URL); break;
            case Constants.FEED_URL: this.changeURL(Constants.FEED_URL); break;
            case Constants.EVENTS_URL: this.changeURL(Constants.EVENTS_URL); break;
            case Constants.ABOUTUS_URL: this.changeURL(Constants.ABOUTUS_URL); break;
            case Constants.NOTIFICATIONS_URL: this.changeURL(Constants.NOTIFICATIONS_URL); break;
            case Constants.INBOX_URL: this.changeURL(Constants.INBOX_URL); break;
            case Constants.BACKUPS_URL: this.changeURL(Constants.BACKUPS_URL); break;
            case Constants.API_URL: this.changeURL(Constants.API_URL); break;
            
            case 'signout': this.handleSignOut(); break;
            default: break;
        }
    }

    changeURL(url) {
        this.props.history.push(`/${url}`);
        console.log(url)
    }

    handleSignOut = () => {
        //go to / (root)
        this.props.history.push('/');
        //reload
        window.location.reload();
    }

    render() {
        const styles = {
            title: {
              cursor: 'pointer',
              color: Colors.white
            },
            bgColor: {
                backgroundColor: Colors.blue500,
            }
          };

        return(
            <div>
                
                <AppBar
                    id="app-bar"
                    style={styles.bgColor}
                    title={<span style={styles.title}>{Constants.APP_NAME}</span>}
                    onTitleTouchTap={this.handleHomeTouch}
                    iconElementLeft={<IconButton id="menu-icon" className={this.state.headerMenuIconStyle} onClick={this.handleToggle}> <MenuIcon color={Colors.black}/> </IconButton>}
                />

                <SideNav handleToggle={this.handleToggle} handleClose={this.handleClose} sideNavOpen={this.state.sideNavOpen}/>

            </div>
        )
    }
}
export default withRouter(NavBar);