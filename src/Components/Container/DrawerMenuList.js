import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
//ui
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import ArrowLeftIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import DashboardIcon from 'material-ui/svg-icons/action/dashboard';
import MediaIcon from 'material-ui/svg-icons/action/perm-media';
import FeedIcon from 'material-ui/svg-icons/communication/rss-feed';
import EventsIcon from 'material-ui/svg-icons/action/event';
import AboutUsIcon from 'material-ui/svg-icons/social/people';
import InboxIcon from 'material-ui/svg-icons/content/inbox';
import BackupsIcon from 'material-ui/svg-icons/action/backup';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import LockOpenIcon from 'material-ui/svg-icons/action/lock-open';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import Avatar from 'material-ui/Avatar';
import * as Colors from 'material-ui/styles/colors';
//constants
import Constants from './../../Utils/Constants';

function MenuList(props) {
    return(
        <List>
            <MenuHeader handleToggle={props.handleToggle}/>
            <Divider/>
            <MenuBody handleClose={props.handleClose} activeItem={props.activeItem} sideNavOpen={props.sideNavOpen}/>
        </List>
    )
}

function MenuHeader(props) {
    return(
        <div className="menu-header">
           {/* <div> <span><strong>Menu</strong></span> </div>
            
            <Avatar 
            icon={<AccountCircle />} 
            color={Colors.white}
            backgroundColor={Colors.grey700}
            size={50}
            style={{margin: 16}}/>

            <span style={{fontSize: 14, margin: 8}}>Super Admin</span> */}

            <div style={{height: Constants.SIDENAV_MENU_HEADER_HEIGHT}} className="side-nav-header">
                <div className="pure-g display-flex-center" style={{width: `100%`}}>
                    <div className="pure-u-20-24 display-flex-center">
                        <span><strong>Menu</strong></span>
                    </div>

                    <div className="pure-u-4-24 display-flex-center">
                        <IconButton onClick={props.handleToggle}> <ArrowLeftIcon color={Colors.black}/> </IconButton>
                    </div>
                </div>
            </div>
        </div>
    )
}

function MenuBody(props) {
    let DashboardColor, MediaColor,FeedColor, EventsColor, AboutUsColor, NotificationsColor, InboxColor, BackupsColor, ApiColor, SignoutColor;
    const greyColor = Colors.grey600;

    switch(props.activeItem) {
        case Constants.DASHBOARD_URL: DashboardColor = Colors.black; MediaColor = FeedColor = EventsColor = AboutUsColor = NotificationsColor = InboxColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.MEDIA_URL: MediaColor = Colors.black; DashboardColor = FeedColor = EventsColor = AboutUsColor = NotificationsColor = InboxColor = BackupsColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.FEED_URL: FeedColor = Colors.black; DashboardColor = MediaColor = EventsColor = AboutUsColor = NotificationsColor = InboxColor = BackupsColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.EVENTS_URL: EventsColor = Colors.black; DashboardColor = MediaColor = FeedColor = AboutUsColor = NotificationsColor = InboxColor = BackupsColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.ABOUTUS_URL: AboutUsColor = Colors.black; DashboardColor = MediaColor = EventsColor = FeedColor = NotificationsColor = InboxColor = BackupsColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.NOTIFICATIONS_URL: NotificationsColor = Colors.black; DashboardColor = MediaColor = EventsColor = AboutUsColor = FeedColor = InboxColor = BackupsColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.INBOX_URL: InboxColor = Colors.black; DashboardColor = MediaColor = EventsColor = AboutUsColor = NotificationsColor = FeedColor = BackupsColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.BACKUPS_URL: BackupsColor = Colors.black; DashboardColor = MediaColor = EventsColor = AboutUsColor = NotificationsColor = FeedColor = InboxColor = ApiColor = SignoutColor = greyColor; break;

        case Constants.API_URL: ApiColor = Colors.black; DashboardColor = MediaColor = EventsColor = AboutUsColor = NotificationsColor = FeedColor = InboxColor = BackupsColor = SignoutColor = greyColor; break;
    }

    //call handleClose with parameter anonymously, to avoid being called when MenuBody(which is child component) is called by parent component
    return(

        <div>
            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Dashboard" : "..."}
            style={{color: DashboardColor}}
            leftIcon={<DashboardIcon color={DashboardColor}/>}
            onClick={() => {props.handleClose(Constants.DASHBOARD_URL)} }/>

            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Media" : "..."}
            style={{color: MediaColor}}
            leftIcon={<MediaIcon color={MediaColor}/>} 
            onClick={() => {props.handleClose(Constants.MEDIA_URL)} }/>

            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Feed" : "..."} 
            style={{color: FeedColor}}
            leftIcon={<FeedIcon color={FeedColor}/>} 
            onClick={() => {props.handleClose(Constants.FEED_URL)} }/>

            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Events" : "..."} 
            style={{color: EventsColor}}
            leftIcon={<EventsIcon color={EventsColor}/>} 
            onClick={() => {props.handleClose(Constants.EVENTS_URL)} }/>

            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "About" : "..."}
            style={{color: AboutUsColor}}
            leftIcon={<AboutUsIcon color={AboutUsColor}/>} 
            onClick={() => {props.handleClose(Constants.ABOUTUS_URL)} }/>
            
            <Divider />
            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Notifications" : "..."}
            style={{color: NotificationsColor}}
            leftIcon={<NotificationsIcon color={NotificationsColor}/>} 
            onClick={() => {props.handleClose(Constants.NOTIFICATIONS_URL)} }/>

            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Inbox" : "..."}
            style={{color: InboxColor}}
            leftIcon={<InboxIcon color={InboxColor}/>} 
            onClick={() => {props.handleClose(Constants.INBOX_URL)} }/>

            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Backups" : "..."}
            style={{color: BackupsColor}}
            leftIcon={<BackupsIcon color={BackupsColor}/>} 
            onClick={() => {props.handleClose(Constants.BACKUPS_URL)} }/>

            <Divider/>
            <ListItem 
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "API" : "..."}
            style={{color: ApiColor}}
            leftIcon={<i className="fas fa-database" style={{color: ApiColor}}></i>} 
            onClick={() => {props.handleClose(Constants.API_URL)} }/>
            {/* <BackupsIcon color={ApiColor}/> */}

            <Divider/>
            <ListItem
            className={props.sideNavOpen ? "side-nav-open-list-item" : "side-nav-close-list-item"}
            primaryText={props.sideNavOpen ? "Signout" : "..."}
            style={{color: SignoutColor}}
            leftIcon={<LockOpenIcon color={SignoutColor}/>} 
            onClick={() => {console.log('signout')} }/>
        </div>
    )
}

class DrawerMenuList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeItem: ''
        }
    }
    componentDidMount() {
        this.handleSelectedIndex(this.props.location.pathname);
    }

    componentWillReceiveProps(nextProps) {
        this.handleSelectedIndex(nextProps.location.pathname);
    }

    handleSelectedIndex(location) {
        switch(location) {
            case '/dashboard' :  this.setState({activeItem: Constants.DASHBOARD_URL}); break;
            case '/media' :  this.setState({activeItem: Constants.MEDIA_URL}); break;
            case '/feed' :  this.setState({activeItem: Constants.FEED_URL}); break;
            case '/events' :  this.setState({activeItem: Constants.EVENTS_URL}); break;
            case '/aboutus' :  this.setState({activeItem: Constants.ABOUTUS_URL}); break;
            case '/notifications' :  this.setState({activeItem: Constants.NOTIFICATIONS_URL}); break;
            case '/inbox' :  this.setState({activeItem: Constants.INBOX_URL}); break;
            case '/backups' :  this.setState({activeItem: Constants.BACKUPS_URL}); break;
        }

    }

    render() {
        return(
            <MenuList handleClose={this.props.handleClose} handleToggle={this.props.handleToggle} activeItem={this.state.activeItem} sideNavOpen={this.props.sideNavOpen}/>
        )
    }
}
export default withRouter(DrawerMenuList);