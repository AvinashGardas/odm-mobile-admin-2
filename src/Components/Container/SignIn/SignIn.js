import React,{Component} from 'react'
import {withRouter} from 'react-router-dom'
//functions
import * as firebaseAuth from '../../Firebase/FirebaseAuthenticationApi'
//ui
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
//components
import Carousel from './../Carousel/WelcomeCarousel'

const SignInView = (props) => {
    const {componentOpacity, errorText, handleInput, handleSignIn, display} = props

    return (
        <div className="display-flex-center" style={{opacity: componentOpacity, transition: '.3s ease', height: `100%`}}>

            <div style={{margin: 32}}>
                <div className="display-flex-center" style={{marginBottom: 16}}>
                    <img src={require('./../images/odm-logo.png')} style={{height: 180}} />
                </div>
                
                <h3 className="dancing-script" style={{marginBottom: 8}}>Open Door Ministries</h3>

                <TextField
                id="email"
                hintText="Your email"
                floatingLabelText="Email"
                onChange={handleInput}
                fullWidth={true} />
                <br />

                <TextField
                id="password"
                hintText="Password"
                floatingLabelText="Password"
                type="password"
                onChange={handleInput}
                fullWidth={true} />
                <br />

                <RaisedButton 
                label="Signin"
                primary={true}
                onTouchTap={handleSignIn}
                fullWidth={true} />
                <br />

                <div style={{backgroundColor: '#e53935', padding: 8, fontSize: 14, color: '#ffffff', display, width: 'auto', marginTop: 8}} 
                className="roboto-sans display-flex-center text-overflow">
                    {errorText}
                </div>
            </div>
        </div>
    )
}

class SignIn extends Component {
    state = {
        errorTextVisibility: false,
        errorText: '',
        email: '',
        password: '',

        componentOpacity: 0,
        componentTime: 0,
        componentTimeLimit: 1000,
        intervalId: null,

        innerHeight: 0
    }

    componentDidMount() {
        //resize
        window.addEventListener("resize", this.onResize)
        window.addEventListener("keydown", this.onKeyPress)

        let intervalId = setInterval(this.updateComponentOpacity, this.state.componentTimeLimit)
        this.setState({intervalId})

        //calculate height
        this.calculateHeight()
    }

    componentWillReceiveProps() {
        //calculate height
        this.calculateHeight()
    }

    componentWillUnmount() {
        //resize
        window.removeEventListener("resize", this.onResize)
        window.removeEventListener("keydown", this.onKeyPress)
        
        clearInterval(this.state.intervalId)
    }

    onKeyPress = (event) => {
        const keyCode = event.keyCode

        //if key is "enter"
        if(keyCode === 13) {
            this.handleSignIn()
        }
    }

    onResize = () => {
        this.calculateHeight()
    }

    updateComponentOpacity = () => {
        //only till 2 seconds update state
        if(this.state.componentTime < 2) {
            this.setState({componentOpacity: 1, componentTime: this.componentTime+1})
        }
    }

    handleSignIn = () => {
        if(this.state.email === '' && this.state.password === '') {
            this.setState({errorTextVisibility: true, errorText: 'Enter email, password!'})
        } else {
            firebaseAuth.doSignInWithEmailAndPassword(this.state.email, this.state.password)
            .then(user => {
                sessionStorage.setItem('currentLoggedInEmail', this.state.email)
                this.setState({errorText: '', email: '', password: '', errorTextVisibility: false})
                this.props.history.push('/dashboard')
            })
            .catch(error => {
                this.setState({errorTextVisibility: true, errorText: error.message})
            })
        }
    }

    handleInput = (event) => {
        const id = event.target.id
        const value = event.target.value

        this.setState({[id]: value})
    }

    calculateHeight = () => {
        const innerHeight = window.innerHeight - 64
        this.setState({innerHeight})
    }

    render() {
        const display = this.state.errorTextVisibility ? 'block' : 'none'

        return(
            <div className="pure-g" style={{height: this.state.innerHeight}}>

                <div className="pure-u-1 pure-u-md-8-24" style={{height: `100%`}}>
                    <SignInView 
                    componentOpacity={this.state.componentOpacity}
                    errorText={this.state.errorText}
                    handleInput={this.handleInput}
                    handleSignIn={this.handleSignIn}
                    display={display} />
                </div>

                <div className="pure-u-1 pure-u-md-16-24">
                    <Carousel 
                    className="welcome-carousel"
                    componentOpacity={this.state.componentOpacity} />
                </div>
            </div>
        )
    }
}
export default withRouter(SignIn)