import React,{Component} from 'react'
import {withRouter} from 'react-router-dom'
import firebase from 'firebase'
//functions
import * as firebaseAuth from '../../Firebase/FirebaseAuthenticationApi'
//ui
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
//components
import Carousel from './../Carousel/WelcomeCarousel'

const SignUpView = (props) => {
    const {componentOpacity, errorText, handleInput, handleSignUp, display, backgroundColor} = props

    return (
        <div className="display-flex-center" style={{opacity: componentOpacity, transition: '.3s ease', height: `100%`}}>

            <div style={{margin: 32}}>
                <div className="display-flex-center" style={{marginBottom: 16}}>
                    <img src={require('./../images/odm-logo.png')} style={{height: 180}} />
                </div>
                
                <h3 className="dancing-script" style={{marginBottom: 8}}>Open Door Ministries</h3>

                <TextField
                id="name"
                hintText="Your name"
                floatingLabelText="Name"
                onChange={handleInput}
                fullWidth={true} />
                <br />

                <TextField
                id="email"
                hintText="Your email"
                floatingLabelText="Email"
                onChange={handleInput}
                fullWidth={true} />
                <br />

                <TextField
                id="password"
                hintText="Enter password"
                floatingLabelText="Password"
                type="password"
                onChange={handleInput}
                fullWidth={true} />
                <br />

                <TextField
                id="phone"
                hintText="Enter phone number"
                floatingLabelText="Phone number"
                type="tel"
                pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                onChange={handleInput}
                fullWidth={true} />
                <br />

                <RaisedButton 
                label="SignUp"
                primary={true}
                onTouchTap={handleSignUp}
                fullWidth={true} />
                <br />

                <div style={{backgroundColor, padding: 8, fontSize: 14, color: '#ffffff', display, width: 'auto', marginTop: 8}} 
                className="roboto-sans display-flex-center text-overflow">
                    {errorText}
                </div>
            </div>
        </div>
    )
}

class SignUp extends Component {
    state = {
        errorTextVisibility: false,
        errorText: '',
        errorTextBackgroundColor: '',
        emailExists: false,
        userCreated: false,

        email: '',
        password: '',
        phone: '',
        username: '',

        componentOpacity: 0,
        componentTime: 0,
        componentTimeLimit: 1000,
        intervalId: null,

        innerHeight: 0
    }

    componentDidMount() {
        //resize
        window.addEventListener("resize", this.onResize)
        window.addEventListener("keydown", this.onKeyPress)

        let intervalId = setInterval(this.updateComponentOpacity, this.state.componentTimeLimit)
        this.setState({intervalId})

        //calculate height
        this.calculateHeight()
    }

    componentWillReceiveProps() {
        //calculate height
        this.calculateHeight()
    }

    componentWillUnmount() {
        //resize
        window.removeEventListener("resize", this.onResize)
        window.removeEventListener("keydown", this.onKeyPress)
        
        clearInterval(this.state.intervalId)
    }

    onKeyPress = (event) => {
        const keyCode = event.keyCode

        //if key is "enter"
        if(keyCode === 13) {
            this.handleSignUp()
        }
    }

    onResize = () => {
        this.calculateHeight()
    }

    updateComponentOpacity = () => {
        //only till 2 seconds update state
        if(this.state.componentTime < 2) {
            this.setState({componentOpacity: 1, componentTime: this.componentTime+1})
        }
    }

    handleSignUp = () => {
        //reset before verify
        this.setState({errorTextBackgroundColor: '#e53935', errorTextVisibility: false, errorText: '',emailExists: false})

        if(this.state.email === '' || this.state.password === '' || this.state.name === '' || this.state.phone === '') {
            this.setState({errorTextVisibility: true, errorText: 'Please enter all the above fields!'})
        }
        else {
            //check
            this.checkIfAdminEmailAlreadyExists(this.state.email)
        }
    }

    handleInput = (event) => {
        const id = event.target.id
        const value = event.target.value

        this.setState({[id]: value})
    }

    calculateHeight = () => {
        const innerHeight = window.innerHeight - 64
        this.setState({innerHeight})
    }

    /* firebase */
    checkIfAdminEmailAlreadyExists = (email) => {
        const path = '/admin/adminusers'
        const ref = firebase.database().ref(path)

        ref.once('value', snapshot => {
            const data = snapshot.val()
            let index

            for(index=0; index<data.length; index++) {
                const item = data[index]

                if(item !== undefined) {
                    if(item.email === email) {
                        if(item.status === 'approved') {
                            this.setState({errorTextVisibility: true, errorText: 'The email address is already in use by another account.', emailExists: true})
                        } 
                        else if(item.status === 'pending') {
                            this.setState({errorTextVisibility: true, errorText: 'Request for the email address is already in pending status.', emailExists: true})
                        } 
                        
                    }
                }//if
                
            }//for
            
            //add user only after complete iteration of checking if email already exists
            if(index===data.length && this.state.emailExists !== true) {
                this.addNewAdminUserToPendingQueue(this.state.name, this.state.email, this.state.phone, this.state.password)
                this.setState({errorTextBackgroundColor: '#43A047', errorTextVisibility: true, errorText: 'Your request for the email address has been raised.', emailExists: false})
            }
            
        })
    }

    addNewAdminUserToPendingQueue = (name, email, phone, password) => {
        let latestId = 0
        const path = '/admin/adminusers'

        const calculateRef = firebase.database().ref(path)
        calculateRef.once('value', snapshot => {
            const data = snapshot.val()

            Object.keys(data).map(item => {
                latestId = item
                latestId = parseInt(latestId)
            })

            const latestIdPath = `${path}/${latestId+1}`
            const latestIdRef = firebase.database().ref(latestIdPath)
            const userObject = {
                id: latestId+1,
                name: name,
                email: email,
                password: password,
                phone: phone,
                status: 'pending'
            }
            latestIdRef.set(userObject)
        })
    }

    render() {
        const display = this.state.errorTextVisibility ? 'block' : 'none'
        const backgroundColor = this.state.errorTextBackgroundColor

        return(
            <div className="pure-g" style={{height: this.state.innerHeight}}>

                <div className="pure-u-1 pure-u-md-8-24" style={{height: `100%`}}>
                    <SignUpView 
                    componentOpacity={this.state.componentOpacity}
                    errorText={this.state.errorText}
                    handleInput={this.handleInput}
                    handleSignUp={this.handleSignUp}
                    display={display}
                    backgroundColor={backgroundColor} />
                </div>

                <div className="pure-u-1 pure-u-md-16-24">
                    <Carousel 
                    className="welcome-carousel"
                    componentOpacity={this.state.componentOpacity} />
                </div>
            </div>
        )
    }
}
export default withRouter(SignUp)