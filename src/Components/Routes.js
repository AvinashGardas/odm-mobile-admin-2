import React, {Component} from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import firebase from 'firebase'
//components
import NavigationBar from './Container/Navigation/NavigationBar'
import AccessDenied from './Container/AccessDenied/AccessDenied'
import Welcome from './Content/Welcome/Welcome'
import SignIn from './Container/SignIn/SignIn'
import SignUp from './Container/SignUp/SignUp'
import AppContainer from './Container/AppContainer'
import Dashboard from './Content/Dashboard/Dashboard'
import Media from './Content/Media/Media'
import Feed from './Content/Feed/Feed'
import Events from './Content/Events/Events'
import AboutUs from './Content/AboutUs/AboutUs'
import Notifications from './Content/Notifications/Notifications'
import Inbox from './Content/Inbox/Inbox'
import Backups from './Content/Backups/Backups'
import PageNotFound from './Content/PageNotFound/PageNotFound'
import Example from './Example/Example'
import PrivateRoute from './PrivateRoute'
import DefaultPageLazyLoad from './GenericComponents/LazyLoad/DefaultPageLazyLoad'
//css
import './Content/css/content.css'
//constants
import Constants from './../Utils/Constants'
import * as firebaseAuth from './Firebase/FirebaseAuthenticationApi'
//API
import ApiMedia from './API/media/ApiMedia'
import ApiMediaAudio from './API/media/audio/ApiMediaAudio'
import ApiMediaPhotos from './API/media/photos/ApiMediaPhotos'
import ApiMediaPhotosAlbum from './API/media/photos/album/ApiMediaPhotosAlbum'
import ApiMediaPhotosAlbumItem from './API/media/photos/album/album-item/ApiMediaPhotosAlbumItem'
import ApiMediaPhotosAlbumItem_Photos from './API/media/photos/album/album-item/album-item-photos/ApiMediaPhotosAlbumItem_Photos'
import ApiMediaPhotosAlbumItem_PhotosDetail from './API/media/photos/album/album-item/album-item-photos/photos-detail/ApiMediaPhotosAlbumItem_PhotosDetail'
import ApiMediaVideo from './API/media/video/ApiMediaVideo'
import Api from './API/Api'
import Logout from './Container/Logout/Logout';
import { FlatButton } from 'material-ui';

class Routes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            authenticated: false,
            Loading: true,
        }
    }

    componentDidMount() {
        this.reCheckAuth()
    }
    
    reCheckAuth = () => {
        firebase.auth().onAuthStateChanged(user => {
            if(user !== null) {
                this.setState({user: user.email, authenticated: true, Loading: false})
            } else {
                this.setState({user: null, authenticated: false, Loading: true})
            }
        })
    }

    handleSignOut = () => {
        firebaseAuth.doSignOut().then( () => {this.props.history.push('/')}).catch(error => {console.log(error)})
    }

    render() {
        const { authenticated, Loading, user } = this.state
        const marginTop = (user === null) ? 0 : ''
        const marginLeft = (user === null) ? 0 : ''
        const marginRight = (user === null) ? 0 : ''
        const marginBottom = (user === null) ? 0 : ''

        return(
                <BrowserRouter>
                    
                    <div>
                        
                            <AppContainer />

                            <div id="main-container" style={{marginTop, marginLeft, marginRight, marginBottom}}>
                            <Switch>
                                
                            <Route exact path="/" component={Welcome}/>
                            
                            {Loading ? <Route exact path="/dashboard" component={DefaultPageLazyLoad} /> : 
                (authenticated ? <Route exact path="/dashboard" component={Dashboard} /> : <Route exact path="/dashboard" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/media/:type" component={Media} /> : <Route exact path="/media/:type" component={AccessDenied} />) }

                                <Route  path="/signin" component={SignIn}/>
                                <Route  path="/signup" component={SignUp}/>
                                { (authenticated ? <Route exact path="/feed" component={Feed} /> : <Route exact path="/feed" component={AccessDenied} />) }
                                
                                <Route exact path="/events" component={Events} />
                                <Route exact path="/aboutus" component={AboutUs} />
                                <Route exact path="/notifications" component={Notifications} />
                                <Route exact path="/inbox" component={Inbox} />
                                <Route exact path="/backups" component={Backups} />

                                {(authenticated ? <Route exact path="/api" component={Api} /> : <Route exact path="/api" component={AccessDenied} />) }
                                <Route exact path="/api/media" component={ApiMedia} />
                                <Route exact path="/api/media/audio" component={ApiMediaAudio} />
                                <Route exact path="/api/media/photos" component={ApiMediaPhotos} />
                                <Route exact path="/api/media/photos/album" component={ApiMediaPhotosAlbum} />
                                <Route exact path="/api/media/photos/album/:id" component={ApiMediaPhotosAlbumItem} />
                                <Route exact path="/api/media/photos/album/:id/photos" component={ApiMediaPhotosAlbumItem_Photos} />
                                <Route exact path="/api/media/photos/album/:id/photos/:photoid" component={ApiMediaPhotosAlbumItem_PhotosDetail} />
                                <Route exact path="/api/media/video" component={ApiMediaVideo} />

                                <Route exact path="*" component={PageNotFound} />

                                
                                
                            </Switch>
                            </div>
                        

                        {/* <ul className='simple-nav-ul'>
                            <li><NavLink exact activeClassName="active" to='/'>Home</NavLink></li>
                            {authenticated ? null : <li><NavLink exact to='/signin'>Sign In</NavLink></li>}
                            {authenticated ? null : <li><NavLink exact to='/signup'>Sign Up</NavLink></li>}
                            <li><NavLink exact activeClassName="active" to='/dashboard'>Dashboard</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/media'>Media</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/feed'>Feed</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/events'>Events</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/aboutus'>AboutUs</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/notifications'>Notifications</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/inbox'>Inbox</NavLink></li>
                            <li><NavLink exact activeClassName="active" to='/backups'>Backups</NavLink></li>
                            <li><NavLink activeClassName="active" to='/api'>Api</NavLink></li>
                            {authenticated ?  <li><NavLink activeClassName="active" to='/logout'>Logout</NavLink></li> : null}
                        </ul> */}
                        
                        {/* Switch */}
                        {/* <Switch> */}
                            {/* <Route exact path="/" component={Welcome}/>
                            <Route exact path="/signin" component={SignIn}/>
                            <Route exact path="/signup" component={SignUp}/>
                            <Route exact path="/logout" component={Logout}/>
                            <Route exact path="/experiment" component={Example}/>

                            { (authenticated ? <Route exact path="/example" component={Example} /> : <Route exact path="/example" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/dashboard" component={Dashboard} /> : <Route exact path="/dashboard" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/media" component={Media} /> : <Route exact path="/media" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/feed" component={Feed} /> : <Route exact path="/feed" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/events" component={Events} /> : <Route exact path="/events" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/aboutus" component={AboutUs} /> : <Route exact path="/aboutus" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/notifications" component={Notifications} /> : <Route exact path="/notifications" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/inbox" component={Inbox} /> : <Route exact path="/inbox" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/backups" component={Backups} /> : <Route exact path="/backups" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/api" component={Api} /> : <Route exact path="/api" component={AccessDenied} />) }
                            { (authenticated ? <Route exact path="/api/media" component={ApiMedia} /> : <Route exact path="/api" component={AccessDenied} />) }
                            { (authenticated ? <Route exact path="/api/media/audio" component={ApiMediaAudio} /> : <Route exact path="/api" component={AccessDenied} />) }
                
                            { (authenticated ? <Route exact path="/api/media/video" component={ApiMediaVideo} /> : <Route exact path="/api" component={AccessDenied} />) }

                            { (authenticated ? <Route exact path="/api/media/photos" component={ApiMediaPhotos} /> : <Route exact path="/api" component={AccessDenied} />) }
                            { (authenticated ? <Route exact path="/api/media/photos/album" component={ApiMediaPhotosAlbum} /> : <Route exact path="/api" component={AccessDenied} />) }
                            { (authenticated ? <Route exact path="/api/media/photos/album/:id" component={ApiMediaPhotosAlbumItem} /> : <Route exact path="/api" component={AccessDenied} />) }
                            { (authenticated ? <Route exact path="/api/media/photos/album/:id/photos" component={ApiMediaPhotosAlbumItem_Photos} /> : <Route exact path="/api" component={AccessDenied} />) }
                            { (authenticated ? <Route exact path="/api/media/photos/album/:id/photos/:photoid" component={ApiMediaPhotosAlbumItem_PhotosDetail} /> : <Route exact path="/api" component={AccessDenied} />) } */}

                            {/* <Route exact path="*" component={PageNotFound} /> */}

                            {/* <BrowserRouter>
                                <div id="main-container" style={{width: `${Constants.DefaultMainContainerMarginLeft}`}}>

                                    <div >
                                        <Switch>
                                        {Loading ? <Route exact path="/dashboard" component={DefaultPageLazyLoad} /> : 
                            (authenticated ? <Route exact path="/dashboard" component={Dashboard} /> : <Route exact path="/dashboard" component={AccessDenied} />) }

                                        { (authenticated ? <Route exact path="/media" component={Media} /> : <Route exact path="/media" component={AccessDenied} />) }

                                            <Route exact path="/media" component={Media} />
                                            <Route exact path="/feed" component={Feed} />
                                            <Route exact path="/events" component={Events} />
                                            <Route exact path="/aboutus" component={AboutUs} />
                                            <Route exact path="/notifications" component={Notifications} />
                                            <Route exact path="/inbox" component={Inbox} />
                                            <Route exact path="/backups" component={Backups} />

                                            {(authenticated ? <Route exact path="/api" component={Api} /> : <Route exact path="/api" component={AccessDenied} />) }
                                            <Route exact path="/api/media" component={ApiMedia} />
                                            <Route exact path="/api/media/audio" component={ApiMediaAudio} />
                                            <Route exact path="/api/media/photos" component={ApiMediaPhotos} />
                                            <Route exact path="/api/media/photos/album" component={ApiMediaPhotosAlbum} />
                                            <Route exact path="/api/media/photos/album/:id" component={ApiMediaPhotosAlbumItem} />
                                            <Route exact path="/api/media/photos/album/:id/photos" component={ApiMediaPhotosAlbumItem_Photos} />
                                            <Route exact path="/api/media/photos/album/:id/photos/:photoid" component={ApiMediaPhotosAlbumItem_PhotosDetail} />
                                            <Route exact path="/api/media/video" component={ApiMediaVideo} />

                                            <Route exact path="*" component={PageNotFound} />
                                        </Switch>
                                    </div>

                                </div>
                            </BrowserRouter> */}

                        {/* </Switch> */}
                        </div>
                    
                </BrowserRouter>            
        )
    }
}
export default Routes