import React, {Component} from 'react'
//ui
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'

class AddToDo extends Component {
    state = {
        todo: ''
    }

    handleInput = (event) => {
        const id = event.target.id
        const value = event.target.value

        this.setState({[id]: value})
    }

    render() {
        const {overlayOpen, closeOverlay, submitButtonOverlay} = this.props

        const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={closeOverlay}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              onClick={ () => {submitButtonOverlay(this.state.todo)} }
            />
          ]

        return(
            <div>
                <Dialog
                title="Add notes"
                actions={actions}
                modal={true}
                open={overlayOpen}>

                    <TextField
                    id="todo"
                    hintText="Type here"
                    floatingLabelText="To Do"
                    type="text"
                    onChange={this.handleInput}
                    fullWidth={true} />
                    <br />
                </Dialog>
                
            </div>
        )
    }
}

export default AddToDo