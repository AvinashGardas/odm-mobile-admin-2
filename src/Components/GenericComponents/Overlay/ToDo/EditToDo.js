import React, {Component} from 'react'
//ui
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'

class EditToDo extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          value: this.props.currentData
        }
    }

    componentDidMount() {
        this.setState({value: this.props.currentData})
    }

    componentWillReceiveProps(nextProps) {
        this.setState({value: nextProps.currentData})
    }

    handleInput = (event) => {
        this.setState({
            value: event.target.value
          })
    }

    render() {
        const {overlayOpen, closeOverlay, submitButtonOverlay} = this.props

        const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={closeOverlay}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              onClick={ () => {submitButtonOverlay(this.state.value)} }
            />
          ]

        return(
            <div>
                <Dialog
                title="Edit notes"
                actions={actions}
                modal={true}
                open={overlayOpen}>

                    <TextField
                    id="text-field-controlled"
                    hintText="Type here"
                    floatingLabelText="To Do"
                    type="text"
                    value={this.state.value}
                    onChange={this.handleInput}
                    fullWidth={true} />
                    <br />
                </Dialog>
                
            </div>
        )
    }
}

export default EditToDo