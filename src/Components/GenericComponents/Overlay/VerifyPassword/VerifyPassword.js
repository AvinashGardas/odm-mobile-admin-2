import React, {Component} from 'react'
//ui
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'

class VerfiryPassword extends Component {
    state = {
        password: ''
    }

    handleInput = (event) => {
        const id = event.target.id
        const value = event.target.value

        this.setState({[id]: value})
    }

    render() {
        const {overlayOpen, closeOverlay, submitButtonOverlay, action} = this.props

        const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={closeOverlay}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              onClick={ () => {submitButtonOverlay(this.state.password, action)} }
            />
          ]

        return(
            <div>
                <Dialog
                title="Enter password to complete action"
                actions={actions}
                modal={true}
                open={overlayOpen}>

                    <TextField
                    id="password"
                    hintText="Enter password"
                    floatingLabelText="Password"
                    type="password"
                    onChange={this.handleInput}
                    fullWidth={true} />
                    <br />
                </Dialog>
                
            </div>
        )
    }
}

export default VerfiryPassword