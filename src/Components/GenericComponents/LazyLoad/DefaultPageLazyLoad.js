import React, {Component} from 'react'
//ui
import CircularProgress from 'material-ui/CircularProgress'

class DefaultPageLazyLoad extends Component {
    render() {
        return(
            <div style={{width: `100%`, height:`200px`}} className='display-flex-center' id="page-container">
                <CircularProgress />
            </div>
        )
    }
}
export default DefaultPageLazyLoad