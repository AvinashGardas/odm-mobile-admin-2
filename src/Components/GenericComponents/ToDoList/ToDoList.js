import React, {Component} from 'react'
import firebase from 'firebase'
import * as firebaseAuth from './../../Firebase/FirebaseAuthenticationApi'
//ui
import {Card} from 'material-ui/Card'
import * as Colors from 'material-ui/styles/colors'
import Snackbar from 'material-ui/Snackbar'
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import AddCircleOutlineIcon from 'material-ui/svg-icons/content/add-circle-outline'
import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table'
//comp
import VerifyPassword from './../Overlay/VerifyPassword/VerifyPassword'
import AddToDo from '../Overlay/ToDo/AddToDo'
import EditToDo from '../Overlay/ToDo/EditToDo'

class List extends Component {
    renderList() {
        const {data, editToDo, deleteToDo} = this.props

        if(data!== null) {
            return(
                <Table
                style={{width: 'auto'}}
                fixedHeader={true}
                selectable={false}
                multiSelectable={false}>
                    <TableBody
                    displayRowCheckbox={false}
                    showRowHover={true}>
                        {
                            Object.values(data).map((item, key) => {
                                return(
                                    <TableRow key={key}>
                                        <TableRowColumn style={{overflow: 'visible', whiteSpace: 'pre-line'}}>{item.data}</TableRowColumn>

                                        <TableRowColumn style={{textAlign: 'right'}}>
                                            <IconMenu
                                            iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                                            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                                            targetOrigin={{horizontal: 'left', vertical: 'top'}}
                                            >
                                                <MenuItem primaryText="Edit" onClick={ ()=>{editToDo(item.id, item.data)} } />
                                                <MenuItem primaryText="Delete" onClick={ ()=>{deleteToDo(item.id)} } />
                                            </IconMenu>
                                        </TableRowColumn>
                                    </TableRow>
                                )
                            })
                        }
                    </TableBody>
                </Table>
            )       
        }
    }

    render() {
        const {} = this.props

        return(
            <div>
                {
                    this.renderList()
                }
            </div>
        )
    }
}

const Header = (props) => {
    const {openToDoOverlay} = props

    return(
        <div className="pure-g">
            <div className="pure-u-22-24 display-flex-center">
                <span className="generic-dashboard-card-title display-flex-center">
                    Notes
                </span>
            </div>

            <div className="pure-u-2-24 display-flex-end">
                <IconButton
                onClick={openToDoOverlay}>
                    <AddCircleOutlineIcon />
                </IconButton>
            </div>
        </div>
    )
}

class ToDoList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: null,
            overlayOpen: false,
            todoOverlayOpen: false,
            todoEditOverlayOpen: false,
            snackbarOpen: false,
            snackbarMessage: '',

            currentId: '',
            currentData: '',
        }
    }

    componentDidMount() {
        this.fetchData()
    }

    componentWillReceiveProps() {
        this.fetchData()
    }

    componentWillUnmount() {

    }

    fetchData = () => {
        const path = '/admin/todo'
        const ref = firebase.database().ref(path)
        ref.on('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    /* overlays */
    openOverlay = () => {
        this.setState({overlayOpen: true})
    }

    closeOverlay = () => {
        this.setState({overlayOpen: false})
    }

    openToDoOverlay = () => {
        this.setState({todoOverlayOpen: true})
    }

    closeToDoOverlay = () => {
        this.setState({todoOverlayOpen: false})
    }

    openToDoEditOverlay = () => {
        this.setState({todoEditOverlayOpen: true})
    }

    closeToDoEditOverlay = () => {
        this.setState({todoEditOverlayOpen: false})
    }
    /* overlays */

    /* snackbars */
    openSnackbar = () => {
        this.setState({snackbarOpen: true})
    }

    closeSnackbar = () => {
        this.setState({snackbarOpen: false})
    }
    /* snackbars */

    /* to do list api */
    calculateLatestId = () => {
        let latestId = 0
        const path = '/admin/todo'

        const calculateRef = firebase.database().ref(path)
        calculateRef.once('value', snapshot => {
            const data = snapshot.val()

            if(data!==null) {
                Object.keys(data).map(item => {
                    latestId = item
                    latestId = parseInt(latestId)
                    latestId += 1
                })
            }
            
        })
        return latestId
    }

    addToDoSubmitButtonOverlay = (data) => {
        this.createToDoItem(data)
        this.closeToDoOverlay()
    }

    createToDoItem = (data) => {
        const id = this.calculateLatestId()
        const path = `/admin/todo/${id}`
        const email = sessionStorage.getItem('currentLoggedInEmail')
        const timestamp = new Date().toISOString()

        const ref = firebase.database().ref(path)
        const todo = {
            id: id,
            data: data,
            createdBy: email,
            timestamp: timestamp
        }
        ref.set(todo)
    }

    editToDo = (id, data) => {
        this.setState({currentId: id, currentData: data})
        this.openToDoEditOverlay()
    }

    editToDoSubmitButtonOverlay = (data) => {
        this.editToDoItem(data)
        this.closeToDoEditOverlay()
    }

    editToDoItem = (data) => {
        const id = this.state.currentId
        const path = `/admin/todo/${id}`
        const email = sessionStorage.getItem('currentLoggedInEmail')
        const timestamp = new Date().toISOString()

        const ref = firebase.database().ref(path)
        const todo = {
            id: id,
            data: data,
            createdBy: email,
            timestamp: timestamp
        }
        ref.update(todo)
    }
    
    deleteToDo = (id) => {
        this.setState({currentId: id})
        this.openOverlay()
    }

    deleteToDoItem = () => {
        const id = this.state.currentId
        const path = `/admin/todo/${id}`

        const ref = firebase.database().ref(path)
        ref.remove()
    }

    verifyPasswordSubmitButtonOverlay = (password, action) => {
        this.closeOverlay()

        const email = sessionStorage.getItem('currentLoggedInEmail')
        firebaseAuth.doSignInWithEmailAndPassword(email, password)
                    .then(() => {
                        //delete
                        this.deleteToDoItem()
                    })
                    .catch((error) => {
                        this.setState({snackbarMessage: error.message})
                        this.openSnackbar()
                    })
    }
    /* to do list api */

    render() {
        return(
            <div>
                <Card style={{padding: 8}}>

                    <Header 
                    openToDoOverlay={this.openToDoOverlay}/>
                    
                    
                    {
                        this.state.data !== null ?
                        <List
                        data={this.state.data}
                        openToDoOverlay={this.openToDoOverlay}
                        editToDo={this.editToDo}
                        deleteToDo={this.deleteToDo} />
                        :
                        <div>
                            <p>List is empty, please add notes...</p>
                        </div>
                    }
                </Card>

                <VerifyPassword 
                overlayOpen={this.state.overlayOpen} 
                closeOverlay={this.closeOverlay}
                submitButtonOverlay={this.verifyPasswordSubmitButtonOverlay} />

                <AddToDo 
                overlayOpen={this.state.todoOverlayOpen} 
                closeOverlay={this.closeToDoOverlay}
                submitButtonOverlay={this.addToDoSubmitButtonOverlay} />

                <EditToDo 
                overlayOpen={this.state.todoEditOverlayOpen} 
                closeOverlay={this.closeToDoEditOverlay}
                submitButtonOverlay={this.editToDoSubmitButtonOverlay}
                currentData={this.state.currentData} />

                <Snackbar
                open={this.state.snackbarOpen}
                message={this.state.snackbarMessage}
                autoHideDuration={4000}
                onRequestClose={this.closeSnackbar} />
            </div>
        )
    }
}

export default ToDoList