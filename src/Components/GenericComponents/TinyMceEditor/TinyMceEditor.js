import React, {Component} from 'react';
import { Editor } from '@tinymce/tinymce-react';
//ui
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import * as Colors from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import ModeEdit from 'material-ui/svg-icons/editor/mode-edit';

//Note: by default react escapes HTML to prevent XSS, use dangerouslySetInnerHTML to render HTML string to DOM

class TinyMceEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      content: '<p>This is the initial content of the editor</p>'
    }
  }

  componentDidMount() {
    this.props.content !== undefined ? this.setState({content: this.props.content}) : this.setState({content: '<p>This is the initial content of the editor</p>'});
  }

  componentWillReceiveProps(nextProps) {
    this.setState({content: nextProps.content})
  }

  handleEditorChange = (e) => {
    console.log('Content was updated:', e.target.getContent());
    this.setState({content: e.target.getContent()});
  }

  handleSaveContent = () => {
    this.setState({openDialog: false});
    //send content to parent component
    //this.props.getContentFromTinyMceEditor(this.state.content);
  }
  
  handleOpenDialog = () => {
    this.setState({openDialog: true});
  }

  handleCloseDialog = () => {
    this.setState({openDialog: false});
  }

  render() {
    const actions = [
                <FlatButton
                    label="Cancel"
                    primary={true}
                    onTouchTap={this.handleCloseDialog}
                    style={{color: Colors.darkBlack}}
                />,
                <FlatButton
                    label="Save"
                    primary={true}
                    onTouchTap={this.handleSaveContent}
                    style={{color: Colors.darkBlack}}
                />,
            ];

    return (
      <div>
        {/* TinyMceEditor icon */}
        <IconButton>
          <ModeEdit onTouchTap={this.handleOpenDialog}/>
        </IconButton>

        {/* color-palette dialog */}
        <Dialog
        title="Editor"
        actions={actions}
        modal={false}
        open={this.state.openDialog}
        onRequestClose={this.handleCloseDialog}
        autoScrollBodyContent={true}>

          <Editor
            content={this.state.content}
            config={{
              selector: 'textarea',  
              plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
              toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
              advlist_bullet_styles: 'default,circle,disc,square'
            }}
            onChange={this.handleEditorChange}
          />

        </Dialog>
        
      </div>
    );
  }
}

export default TinyMceEditor;