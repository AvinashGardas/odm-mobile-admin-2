import React, {Component} from 'react'
import firebase from 'firebase'
import * as firebaseAuth from './../../Firebase/FirebaseAuthenticationApi'
//ui
import {Card} from 'material-ui/Card'
import {red600, green600} from 'material-ui/styles/colors'
import CircularProgress from 'material-ui/CircularProgress'
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table'
import FlatButton from 'material-ui/FlatButton'
import Snackbar from 'material-ui/Snackbar'
//comp
import VerifyPassword from './../../GenericComponents/Overlay/VerifyPassword/VerifyPassword'

class UserRequests extends Component {
    state = {
        data: '',
        overlayOpen: false,
        snackbarOpen: false,
        snackbarMessage: '',
        userId: null,
        userEmail: null,
        userPassword: null,
        action: ''
    }

    componentDidMount() {
        this.fetchData()
    }

    fetchData = () => {
        const path = '/admin/adminusers'
        const ref = firebase.database().ref(path)
        ref.on('value', snapshot => {
            this.setState({data: snapshot.val()})
        })
    }

    approveUser = (id, email, password) => {
        this.setState({userId: id, userEmail: email, userPassword: password, action: 'approve'})
        this.openOverlay()
    }

    disapproveUser = (id, email, password) => {
        this.setState({userId: id, userEmail: email, userPassword: password, action: 'disapprove'})
        this.openOverlay()
    }

    openOverlay = () => {
        this.setState({overlayOpen: true})
    }

    closeOverlay = () => {
        this.setState({overlayOpen: false})
    }

    submitButtonOverlay = (password, action) => {
        this.closeOverlay()
        
        if(action === 'approve') {
            const email = sessionStorage.getItem('currentLoggedInEmail')
            firebaseAuth.doSignInWithEmailAndPassword(email, password)
                        .then(() => {
                            this.createUserAndUpdateStatusToApproved()
                        })
                        .catch((error) => {
                            this.setState({snackbarMessage: error.message})
                            this.openSnackbar()
                        })
        }
        else if(action === 'disapprove') {
            this.updateStatusToPending()
        }
    }

    createUserAndUpdateStatusToApproved = () => {
        firebaseAuth.doCreateUserWithEmailAndPassword(this.state.userEmail, this.state.userPassword)
                    .then((user) => {
                        //update status
                        this.updateStatusToApproved()
                    })
                    .catch((error) => {
                        if(error.code === 'auth/email-already-in-use') {
                            //update status
                            this.updateStatusToApproved()
                        }
                    })
    }

    updateStatusToApproved = () => {
        const path = `/admin/adminusers/${this.state.userId}`
        const ref = firebase.database().ref(path)
        ref.update({
            status: 'approved'
        })

        this.setState({snackbarMessage: 'User is now approved'})
        this.openSnackbar()
    }

    updateStatusToPending = () => {
        const path = `/admin/adminusers/${this.state.userId}`
        const ref = firebase.database().ref(path)
        ref.update({
            status: 'pending'
        })

        this.setState({snackbarMessage: 'User has been disapproved'})
        this.openSnackbar()
    }

    openSnackbar = () => {
        this.setState({snackbarOpen: true})
    }

    closeSnackbar = () => {
        this.setState({snackbarOpen: false})
    }

    renderData() {
        const data = this.state.data

        return(
            Object.values(data)
                .map((item, key) => {
                    return(
                        <TableRow key={key}>
                            <TableRowColumn style={{overflow: 'visible', whiteSpace: 'pre-line'}}>{item.email}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'right'}}>{item.status}</TableRowColumn>
                            <TableRowColumn style={{textAlign: 'right'}}>
                                {
                                    item.status === 'pending' ?
                                    <FlatButton label="Approve" className="button-text-lower-case" style={{color: green600}} onTouchTap={ ()=>{this.approveUser(item.id, item.email, item.password)} } />
                                    :
                                    <FlatButton label="Disapprove" className="button-text-lower-case" style={{color: red600}} onTouchTap={ ()=>{this.disapproveUser(item.id)} } />
                                }
                            </TableRowColumn>
                        </TableRow>
                    )
                })
        )
    }
    
    render() {
        return(
            <div>
                {
                    this.state.data !== '' ?
                    <Card style={{padding: 8}}>
                        <Table
                        fixedHeader={true}
                        selectable={false}
                        multiSelectable={false}>
                            <TableHeader
                                displaySelectAll={false}
                                adjustForCheckbox={false}
                                enableSelectAll={false}>
                                    <TableRow>
                                        <TableHeaderColumn colSpan="3" style={{textAlign: 'center', fontWeight: 'bold', color: '#000', fontSize: 18}}>
                                            User Requests
                                        </TableHeaderColumn>
                                    </TableRow>
                                    <TableRow>
                                        <TableHeaderColumn>Email</TableHeaderColumn>
                                        <TableHeaderColumn style={{textAlign: 'right'}}>Status</TableHeaderColumn>
                                        <TableHeaderColumn></TableHeaderColumn>
                                    </TableRow>
                            </TableHeader>

                            <TableBody
                            displayRowCheckbox={false}
                            showRowHover={true}>
                                {this.renderData()}
                            </TableBody>
                        </Table>
                        
                    </Card>
                    :
                    <CircularProgress />
                }
                
                <VerifyPassword 
                overlayOpen={this.state.overlayOpen} 
                closeOverlay={this.closeOverlay}
                submitButtonOverlay={this.submitButtonOverlay}
                action={this.state.action} />

                <Snackbar
                open={this.state.snackbarOpen}
                message={this.state.snackbarMessage}
                autoHideDuration={4000}
                onRequestClose={this.closeSnackbar}
                />
            </div>
        )
    }
}

export default UserRequests