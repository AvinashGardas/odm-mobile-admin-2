import firebase from 'firebase'

export const doSignInWithEmailAndPassword = (email, password) => {
    return firebase.auth().signInWithEmailAndPassword(email, password)
}

export const doSignOut = () => {
    return firebase.auth().signOut()
}

export const doCreateUserWithEmailAndPassword = (email, password) => {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
}