import firebase from 'firebase'

export const addNewAdminUserToPendingQueue = (name, email, phone) => {
    const path = '/admin/adminusers/pending'
    const addNewAdminUserToPendingQueue = firebase.database().ref(path)
    return addNewAdminUserToPendingQueue
}